QT				+=	core gui network websockets

greaterThan(QT_MAJOR_VERSION, 4){
    QT			+=	widgets concurrent printsupport
}

SRC_PATH 		= $$PWD/../src
SRC_HIST_PATH 	= $$PWD/../src/histogram
SRC_MODEL_PATH 	= $$PWD/../src/model
FDPSLIB_PATH	= $$PWD/../../fdpslib

TARGET 			= 	fdps-statistics
TEMPLATE 		= 	app

QWT_ROOT = $${PWD}/../../qwt
include( $$QWT_ROOT/qwt.prf )

INCLUDEPATH 	+=	$$SRC_PATH
INCLUDEPATH 	+=	$$SRC_HIST_PATH
INCLUDEPATH 	+=	$$SRC_MODEL_PATH
INCLUDEPATH 	+=	$$FDPSLIB_PATH
INCLUDEPATH     +=  $$QWT_ROOT/src

include(../../qt-solutions/qtsingleapplication/src/qtsingleapplication.pri)

SOURCES     	+= 	$$FDPSLIB_PATH/netutil.cpp \	
                    $$FDPSLIB_PATH/settings_adapter.cpp \
                    $$SRC_HIST_PATH/HistController.cpp \
					$$SRC_MODEL_PATH/LegendTableModel.cpp \
					$$SRC_MODEL_PATH/PlansTableModel.cpp \
					$$SRC_PATH/main.cpp\
					$$SRC_PATH/MainWindow.cpp \				
					$$SRC_PATH/WsClient.cpp \
                    $$SRC_PATH/WsClientController.cpp
	
HEADERS     	+=	$$FDPSLIB_PATH/netutil.h \
                    $$FDPSLIB_PATH/settings_adapter.h \
                    $$SRC_HIST_PATH/HistController.h \
                    $$SRC_HIST_PATH/HistCountAxis.h \
                    $$SRC_HIST_PATH/HistTimePicker.h \
                    $$SRC_HIST_PATH/HistPlansItem.h \
                    $$SRC_HIST_PATH/HistPlansPicker.h \
                    $$SRC_HIST_PATH/HistTimeAxis.h \
                    $$SRC_HIST_PATH/HistNpsZoneItem.h \
                    $$SRC_HIST_PATH/HistZoomer.h \					
					$$SRC_MODEL_PATH/LegendTableModel.h \
					$$SRC_MODEL_PATH/PlansTableModel.h \
					$$SRC_MODEL_PATH/SpinBoxDelegate.hpp \
					$$SRC_MODEL_PATH/TableModelUtils.h \
					$$SRC_PATH/AppSettings.h \
					$$SRC_PATH/LegendItem.h \
					$$SRC_PATH/MainWindow.h \
                    $$SRC_PATH/PlanInfo.h \
					$$SRC_PATH/SettsSingle.h \				
					$$SRC_PATH/UtilWidgetParams.h \
					$$SRC_PATH/UtilWidgetPos.h \
					$$SRC_PATH/WsClient.h \
                    $$SRC_PATH/WsClientController.h \
					$$SRC_PATH/WsProtocol.h \
					$$SRC_PATH/WsClientSettings.h
					
				
RESOURCES   	+=	$$PWD/../icons/icons.qrc
		
FORMS       	+=	$$PWD/../forms/MainWindow.ui

CONFIG(debug, debug|release) {
        LIBS += -L$$QWT_ROOT/lib -lqwtd
}
else {
    LIBS += -L$$QWT_ROOT/lib -lqwt
}
