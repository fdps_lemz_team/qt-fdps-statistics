#pragma once

#include <QAbstractItemModel>
#include <QString>
#include <QTableView>
#include <QHeaderView>


namespace Fdps {
    namespace statistics {

        /// Инициализация виджета QTableView моделью.
        inline void prepareTableView(QTableView* view, QAbstractItemModel* model = 0, bool sortEnable = false)
        {
            view->setSortingEnabled(sortEnable);
            view->verticalHeader()->setDefaultSectionSize(20);
            view->verticalHeader()->setVisible(false);
            view->horizontalHeader()->setDefaultSectionSize(150);
            view->horizontalHeader()->setDefaultAlignment(Qt::AlignHCenter);
            view->horizontalHeader()->setStretchLastSection(true);
            //view->setSelectionBehavior(QAbstractItemView::SelectRows);
            //view->setSelectionMode(QAbstractItemView::SingleSelection);
            if (model)
            {
                view->setModel(model);
            }
        }

		
		inline QString tableToHtml(QAbstractItemModel * model, const QList<int> &excludeColumns) {
			QString html= "<table border=\"1\" cellspacing=\"0\" cellpadding=\"4\">";
			
			for (auto colIt = 0; colIt < model->columnCount(); ++colIt) {
				if (!excludeColumns.contains(colIt)) {

					html += "<tr>";

					html += "<td>";
					html += model->headerData(colIt, Qt::Horizontal, Qt::DisplayRole).toString();
					html += "</td>";

					for (auto rowIt = 0; rowIt < model->rowCount(); ++rowIt) {
						const QColor backColor = model->data(model->index(rowIt, colIt), Qt::BackgroundRole).value<QColor>();
						if (backColor.isValid()) {
							html += QString("<td  bgcolor=\"%1\" align=\"center\">")
								.arg(backColor.name());
						} else {
							html += "<td align=\"center\">";
						}

						html += QString("<font size=\"4\" color=\"black\" face=\"verdana\">%1</font>")
							.arg(model->data(model->index(rowIt, colIt), Qt::DisplayRole).toString());
						html += "</td>";
					}
				}
			}
			html += "</tr>";
			html += "</table></body></html>";

			return html;
		}
    }
}


