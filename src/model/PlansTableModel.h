#pragma once 

#include <QAbstractTableModel>

#include "LegendItem.h"


namespace Fdps {
	namespace statistics {

		const int PlansNameColumn = 0;
		const int PlansNpsColumn = 2;

		/// Модель отображения списка РЛС для окна настроек.
		class PlansTableModel : public QAbstractTableModel {
			Q_OBJECT
		public:
			PlansTableModel(QObject* parent);

			virtual int columnCount(const QModelIndex&) const;

			virtual int rowCount(const QModelIndex&) const;

			virtual QVariant data(const QModelIndex& index, int role) const;

			bool setData(const QModelIndex& index, const QVariant& value, int role);

			virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

			Qt::ItemFlags flags(const QModelIndex& index) const;

			void setLegendItemList(const LegendItemList & legendItems, const LegendType & legendType);

			void setColorNps(const bool &colorNps);

			QString contentToString() const;


		Q_SIGNALS:
			void itemChanged(const LegendItem& item);

		private:
			LegendItemList legendItems_;
			LegendType legendType_;
			bool colorNps_; 
		};
	}
}