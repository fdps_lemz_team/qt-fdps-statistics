#include "PlansTableModel.h"

#include <QtCore/QTextStream>
#include <QtGui/QColor>

#include "SettsSingle.h"

namespace Fdps {
	namespace statistics {

		const QString npsColor = "#ff1943";

		PlansTableModel::PlansTableModel(QObject* parent)
			: QAbstractTableModel(parent){
		}


		int PlansTableModel::columnCount(const QModelIndex&) const {
			return 27;
		}


		int PlansTableModel::rowCount(const QModelIndex&) const {
			return legendItems_.size();
		}


		QVariant PlansTableModel::data(const QModelIndex& index, int role) const {
			if (index.row() < legendItems_.size()) {
				const auto curItem = legendItems_.at(index.row());

				if (role == Qt::DisplayRole) {
					switch (index.column())
					{
					case 0:
						return curItem.name;
					case 1:
						return curItem.histTotal;
					case 2:		
						return curItem.nps;
					case 3:						
					case 4:						
					case 5:
					case 6:
					case 7:
					case 8:
					case 9:
					case 10:
					case 11:
					case 12:
					case 13:
					case 14:
					case 15:
					case 16:
					case 17:
					case 18:
					case 19:
					case 20:
					case 21:
					case 22:
					case 23:
					case 24:
					case 25:
					case 26:
						const int curValue = ( ( index.column() - 3 ) < curItem.histData.size() ? curItem.histData.at(index.column() - 3) : 0);
						return (curValue > 0) ? QVariant(curValue) : QVariant("");
					/*default:
						return QVariant();*/
					}
				}

				if (role == Qt::EditRole) {
					if (index.column() == PlansNpsColumn) {
						return curItem.nps;
					}
				}

				if (role == Qt::BackgroundRole) {

					switch (index.column()) {
					case 3:
					case 4:
					case 5:
					case 6:
					case 7:
					case 8:
					case 9:
					case 10:
					case 11:
					case 12:
					case 13:
					case 14:
					case 15:
					case 16:
					case 17:
					case 18:
					case 19:
					case 20:
					case 21:
					case 22:
					case 23:
					case 24:
					case 25:
					case 26:
						if ((index.column() - 3) < curItem.histData.size()) {
							if (colorNps_ && curItem.nps != defNps) {
								return curItem.histData.at(index.column() - 3) > curItem.nps ?
									QColor(npsColor) : QVariant();
							}							
						}
					default:
						return QVariant();
					}
				}

				if (role == Qt::TextAlignmentRole) {
					return (index.column() > PlansNameColumn) ?
						Qt::AlignCenter : Qt::AlignLeft;
				}
			}

			return QVariant();
		}


		bool PlansTableModel::setData(const QModelIndex& index, const QVariant& value, int role) {
			if (index.isValid() && role == Qt::EditRole) {
				auto curData = legendItems_.at(index.row());

				if (index.column() == PlansNpsColumn) {				
					curData.nps = value.toInt();
					Q_EMIT itemChanged(curData);
				}
				legendItems_.replace(index.row(), curData);
				emit dataChanged(index, index, { Qt::DisplayRole, Qt::EditRole });

				return true;
			}

			return false;
		}


		QVariant PlansTableModel::headerData(int section, Qt::Orientation orientation, int role) const {
			if (orientation == Qt::Horizontal && role == Qt::DisplayRole) {
				switch (section) {
				default:
					return "";
				case 0:
					return legendTypeToString(legendType_);
				case 1:
					return"Всего";
				case 2:
					return"НПС";
				case 3:
				case 4:
				case 5:
				case 6:
				case 7:
				case 8:
				case 9:
				case 10:
				case 11:
				case 12:
				case 13:
				case 14:
				case 15:
				case 16:
				case 17:
				case 18:
				case 19:
				case 20:
				case 21:
				case 22:
				case 23:
				case 24:
				case 25:
					return QString("%1-%2").arg(section - 3).arg(section -2);
				case 26:
					return"23-00";
				}
			}
			return QVariant();
		}


		Qt::ItemFlags PlansTableModel::flags(const QModelIndex& index) const {
			if (!index.isValid())
				return Qt::ItemIsEnabled;

			return index.column() == PlansNpsColumn ?
				QAbstractTableModel::flags(index) | Qt::ItemIsEditable :
				QAbstractTableModel::flags(index) | ~Qt::ItemIsEditable;
		}


		void PlansTableModel::setLegendItemList(const LegendItemList & legendItems, const LegendType & legendType) {
			beginResetModel();
			legendItems_ = legendItems;
			legendType_ = legendType;
			endResetModel();
		}


		void PlansTableModel::setColorNps(const bool& colorNps) {
			colorNps_ = colorNps;
		}


		QString PlansTableModel::contentToString() const {
			QString result;
			QTextStream stream(&result, QIODevice::WriteOnly);

			for (auto nn = 0; nn < 24; ++nn) {
				stream << QString("%1-%2").arg(nn).arg(nn+1) << "\t\t";
			}


			for (auto legentIt = 0; legentIt < legendItems_.size(); ++legentIt) {
				stream << "\n";
				for (auto nn = 0; nn < 24; ++nn) {					
					if (nn < legendItems_.at(legentIt).histData.size()) {
						stream << QString("%1").arg(legendItems_.at(legentIt).histData.at(nn)) << "\t\t";
					}
				}
			}
			
			return result;
		}
	}
}