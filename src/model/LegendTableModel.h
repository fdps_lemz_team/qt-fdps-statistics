#pragma once


#include <QAbstractTableModel>
#include <QColor>

#include "LegendItem.h"

namespace Fdps {
    namespace statistics {

		const int LegendNameColumn = 0;
		const int LegendColorColumn = 1;
		const int LegendNpsColumn = 2;
		const int LegendMaxPsColumn = 3;

        class LegendTableModel : public QAbstractTableModel
        {
            Q_OBJECT
        public:

            LegendTableModel(QObject* parent);
            
            void setLegendItemList(const LegendItemList& data);

            int rowCount(const QModelIndex&) const;

            int columnCount(const QModelIndex&) const;

            QVariant data(const QModelIndex& index, int role) const;

            bool setData(const QModelIndex& index, const QVariant& value, int role);

            QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

            Qt::ItemFlags flags(const QModelIndex& index) const;

            LegendItem getItemByIndex(const QModelIndex& index) const;

        Q_SIGNALS:
            void itemChanged(const LegendItem & item);

        private:
            LegendItemList data_;
        };
    }
}