#include "LegendTableModel.h"


namespace Fdps {
    namespace statistics {

        const QString npsColor = "#ff1943";

        LegendTableModel::LegendTableModel(QObject* parent)
            : QAbstractTableModel(parent)
        {
        }


        void LegendTableModel::setLegendItemList(const LegendItemList& data)
        {
            beginResetModel();
            data_ = data;
            endResetModel();
        }


        int LegendTableModel::rowCount(const QModelIndex&) const
        {
            return data_.size();
        }


        int LegendTableModel::columnCount(const QModelIndex&) const
        {
            return 4;
        }


        QVariant LegendTableModel::data(const QModelIndex& index, int role) const
        {
            if (index.isValid()
                && index.row() >= 0
                && index.row() < data_.size())
            {
                const auto curData = data_.at(index.row());

                if (role == Qt::DisplayRole)
                {
                    switch (index.column())
                    {
                    default:
                        return "";
                    case LegendNameColumn:
                        return curData.name;
                        break;
                    case LegendNpsColumn:
                        return (curData.nps == defNps) ? "-" : QString::number(curData.nps);
                        break;
                    case LegendMaxPsColumn:
                        return (curData.curMaxPs == defNps) ? "-" : QString::number(curData.curMaxPs);
                        break;
                    }
                }

                if (role == Qt::CheckStateRole)
                {
                    if (index.column() == LegendNameColumn) {
                        return curData.isChecked ? Qt::Checked : Qt::Unchecked;
                    }
					if (index.column() == LegendMaxPsColumn) {
						return curData.npsChecked ? Qt::Checked : Qt::Unchecked;
					}
                }

                if (role == Qt::EditRole)
                {
                    if (index.column() == LegendNpsColumn) {
                        return curData.nps;
                    }
                }

                if (role == Qt::BackgroundRole)
                {
                    if (index.column() == LegendColorColumn) {
                        return QColor(curData.color);
                    }
                    if (index.column() == LegendMaxPsColumn) {
                        return curData.curMaxPs > curData.nps ? QColor(npsColor) : QVariant();
                    }
                }
            }
            return QVariant();
        }


        bool LegendTableModel::setData(const QModelIndex& index, const QVariant& value, int role)
        {
            if (index.isValid() && role == Qt::EditRole) {
                auto curData = data_.at(index.row());

                switch (index.column()) {
                case LegendNameColumn:
                    curData.isChecked = value.toBool();
                    break;
                case LegendColorColumn:
                    curData.color = value.toString();
                    break;
                case LegendNpsColumn:
                    curData.nps = value.toInt();
                    Q_EMIT itemChanged(curData);
                    break;
				case LegendMaxPsColumn:
					curData.npsChecked = value.toBool();
					break;
                default:
                    return false;
                }
                data_.replace(index.row(), curData);
                emit dataChanged(index, index, { Qt::DisplayRole, Qt::EditRole });

                return true;
            }

            return false;
        }


        QVariant LegendTableModel::headerData(int section, Qt::Orientation orientation, int role) const
        {
            if (orientation == Qt::Horizontal
                && role == Qt::DisplayRole)
            {
                switch (section)
                {
                default:
                    return tr("");
                case LegendNameColumn:
                    return tr("Код");
                case LegendColorColumn:
                    return tr("Цвет");
                case LegendNpsColumn:
                    return tr("НПС");
                case LegendMaxPsColumn:
                    return tr("max ПС");
                }
            }
            return QVariant();
        }


        Qt::ItemFlags LegendTableModel::flags(const QModelIndex& index) const
        {
            if (!index.isValid())
                return Qt::ItemIsEnabled;

            return index.column() == LegendNpsColumn ?
				QAbstractTableModel::flags(index) | Qt::ItemIsEditable : 
				QAbstractTableModel::flags(index) | ~Qt::ItemIsEditable;
        }


        LegendItem LegendTableModel::getItemByIndex(const QModelIndex& index) const {
            if (index.isValid()
                && index.row() >= 0
                && index.row() < data_.size())
            {
                return data_.at(index.row());
            }
            return LegendItem();
        }
    }
}