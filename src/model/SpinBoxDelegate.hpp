#pragma once 

#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStyledItemDelegate>


namespace Fdps {
    namespace statistics {

        class SpinBoxDelegate : public QStyledItemDelegate
        {
            Q_OBJECT

        public:
            SpinBoxDelegate(QObject* parent = nullptr)
                : QStyledItemDelegate(parent) {
            }
			

            QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& option,
				const QModelIndex& index) const override {

				Q_UNUSED(option);
				Q_UNUSED(index);
				
				QSpinBox* editor = new QSpinBox(parent);
                editor->setFrame(false);
                editor->setMinimum(0);
                editor->setMaximum(1000);

                return editor;
            }


            void setEditorData(QWidget* editor, const QModelIndex& index) const override {
                int value = index.model()->data(index, Qt::EditRole).toInt();

                QSpinBox* spinBox = static_cast<QSpinBox*>(editor);
                spinBox->setValue(value);
            }


            void setModelData(QWidget* editor, QAbstractItemModel* model,
                const QModelIndex& index) const override {
                QSpinBox* spinBox = static_cast<QSpinBox*>(editor);
                spinBox->interpretText();
                int value = spinBox->value();

                model->setData(index, value, Qt::EditRole);
            }

            void updateEditorGeometry(QWidget* editor, const QStyleOptionViewItem& option,
				const QModelIndex& index) const override {

				Q_UNUSED(index);
                editor->setGeometry(option.rect);
            }
        };
    }
}
