#pragma once

#include <QtCore/QCoreApplication>
#include <QtCore/QFile>
#include <QtCore/QJsonDocument>
#include <QtCore/QString>
#include <QtCore/QUuid>

#include "AppSettings.h"
#include "LegendItem.h"


namespace Fdps {
    namespace statistics {



        /// класс для хранения и предоставления настроек приложения
        class SettsSingle : public QObject {
            Q_OBJECT

        private:
            SettsSingle()
                : settingsFilePath_(QCoreApplication::applicationDirPath() + "/settings.json")
				, appInstanceUniqueKey_(QUuid::createUuid().toString())
				, appInstanceVersion_("2020.05.07")
				, appInstanceName_("FDPS-PARKINGS"){

                readSettingsFromFile();
            }

            void readSettingsFromFile() {
                QFile file(settingsFilePath_);
                if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
                    // настройки по умолчанию
                    appSettings_ = AppSettings::fromJsonObject(QJsonObject());
                    writeSettingsToFile();
                    return;
                }
                QJsonDocument jsonDoc = QJsonDocument::fromJson(file.readAll());
                file.close();

                appSettings_ = AppSettings::fromJsonObject(jsonDoc.object());

                if (!jsonDoc.isObject()) {
                    writeSettingsToFile();
                }
            }

            void writeSettingsToFile() {
                QFile file(settingsFilePath_);
                if (!file.open(QIODevice::WriteOnly | QIODevice::Truncate)) {
                    return;
                }

                file.write(QJsonDocument(appSettings_.toJsonObject()).toJson());
                file.close();
            }

            static SettsSingle* instance_;
            AppSettings appSettings_;
            const QString settingsFilePath_;
			const QString appInstanceUniqueKey_;
			const QString appInstanceVersion_;
			const QString appInstanceName_;

        public:
            static SettsSingle* instance() {
                if (!instance_)
                    instance_ = new SettsSingle();
                return instance_;
            }

            AppSettings getAppSetts() const {
                return appSettings_;
            }

            void setAppSetts(const AppSettings& appSetts) {
                appSettings_.wsSetts_ = appSetts.wsSetts_;

                writeSettingsToFile();
            }

			void setGridVisible(const bool& gridVisible) {
				appSettings_.showGrid_ = gridVisible;

				writeSettingsToFile();
			}

            void updateLegendItem(const LegendItem& item, const LegendType& itemType) {

                if (itemType == LegendType::LegendRc) {
                    appSettings_.rcLegendSetts_.setLegendItem(item);
                } else if (itemType == LegendType::LegendAirport) {
                    appSettings_.airportLegendSetts_.setLegendItem(item);
                } else if (itemType == LegendType::LegendAirway) {
                    appSettings_.airwayLegendSetts_.setLegendItem(item);
                } else if (itemType == LegendType::LegendPoint) {
                    appSettings_.pointLegendSetts_.setLegendItem(item);
                }
            }

			void saveSettings() {
				writeSettingsToFile();
			}

			QString AppName() const {
				return appInstanceName_;
			}

			QString AppKey() const {
				return appInstanceUniqueKey_;
			}

			QString AppVersion() const {
				return appInstanceVersion_;
			}			
        };       
    }
}
