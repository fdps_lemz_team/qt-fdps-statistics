#pragma once

#include <QtCore/QJsonDocument>
#include <QtCore/QJsonObject>
#include <QtCore/QByteArray>
#include <QtCore/QString>

#include "SettsSingle.h"
#include "netutil.h"


namespace Fdps {
	namespace statistics {

		namespace json_defs {
			const QString HeaderKey = "MessageType";

			const QString RequestPlansHdr = "RequestPlans";
			const QString DateKey = "Date";
			const QString DateFormat = "yyyy-MM-dd";

			const QString AnswerPlansHdr = "AnswerPlans";	
			const QString PlansKey = "PlansInfo";

			const QString ClientHeartbeatHdr = "ClientHeartbeat";
			const QString AppNameKey = "Name";
			const QString AppVersionKey = "Version";
			const QString AppKeyKey = "Key";
			const QString AppIPsKey = "IPs";
		}


		/// сформировать сообщение запроса планов
		inline QByteArray CreateRequestPlansMsg(const QDate & date) {
			QJsonObject jsonObj;
			jsonObj.insert(json_defs::HeaderKey, QJsonValue(json_defs::RequestPlansHdr));
			jsonObj.insert(json_defs::DateKey, QJsonValue(date.toString(json_defs::DateFormat)));
			return QJsonDocument(jsonObj).toJson();
		}

		/// сформировать сообщение heartbeat
		inline QByteArray CreateClientHeartbeatMsg() {
			QJsonObject jsonObj;
			jsonObj.insert(json_defs::HeaderKey, QJsonValue(json_defs::ClientHeartbeatHdr));
			jsonObj.insert(json_defs::AppNameKey, QJsonValue(SettsSingle::instance()->AppName()));
			jsonObj.insert(json_defs::AppVersionKey, QJsonValue(SettsSingle::instance()->AppVersion()));
			jsonObj.insert(json_defs::AppKeyKey, QJsonValue(SettsSingle::instance()->AppKey()));
			jsonObj.insert(json_defs::AppIPsKey, QJsonArray::fromStringList(Fdps::Net::Util::ip4List()));
			return QJsonDocument(jsonObj).toJson();
		}
	}
}
