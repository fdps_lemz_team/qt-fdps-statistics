#pragma once

#include <QtCore/QTimer>
#include <QtWebSockets/QWebSocket>

#include "PlanInfo.h"
#include "WsClientSettings.h"

namespace Fdps {
    namespace statistics {

        /// клиент для связи с сервисом go
        class WsClient : public QObject {
            Q_OBJECT
        public:
            WsClient(QObject* parent = nullptr);
            void SetParams(const WsClientParams& params);
            ~WsClient();
            
			bool isConnected() const;

        public Q_SLOTS:
            /// 
            void updateParams(const WsClientParams& params);
            /// отправить данные.
            void sendTextMessage(const QByteArray& dataToSend);

        private Q_SLOTS:
            /// Обработка состояний сокета.
            void socketStateChanged(const QAbstractSocket::SocketState& socketState);
            /// сокет удален после вызова deleteLater
            void socketDestroyed();
            /// возникла ошибка в работе сокета
            void socketError(QAbstractSocket::SocketError error);
           
        Q_SIGNALS:      
            /// изменено состояние подключения к серверу
            void stateChaned(bool connected);                 
			/// получены данные
			void receiveData(const QString & data);

        protected:
            /// истек таймер ожидания подключения
            void timerEvent(QTimerEvent* event) override;

        private:
            /// попытка соединения.
            void connectToServer();

            /// удаление сокета
            void destroySocket();

            QWebSocket* webSocket_;                 //!< сокет для связи с сервером RVM.
            QUrl serverUrl_;                        //!< url одключения к серверу RVM.
            const int reconnectInterval_ = 3000;    //!< интервал переподключения к серверу WS (мсек).
            int curCheckConnectTimerId_;            //!< идентификатор аймера для проверки подключения
            const int waitConnectMSecs_ = 10000;     //!< интервал ожидания подключения к cерверу WS (мсек).
            bool isSocketValid_;
			QTimer heartbeatTimer_;
			const int heartbeatIntervalMSecs_ = 3000;
        };
    }
}
