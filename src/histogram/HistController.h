#pragma once

#include <qwt_plot.h>

#include "LegendItem.h"
#include "HistZoomer.h"


class QwtPlotGrid;
class QwtPlotMultiBarChart;
class QwtPlotMarker;


namespace Fdps {
	namespace statistics {

		class PlansPicker;
		class TimePicker;
		class CountAxis;
		class TimeAxis;
		class NpsZoneItem;


		class HistController : public QObject {
			Q_OBJECT

		public:
			HistController(QWidget* parent = 0);

			/// ������ � ��������
			QWidget* getHistWidget() const;

		public Q_SLOTS:
			/// ������� ���� "�����"
			void setGridVisible(bool visible);
						
			/// �������� �������� ������� (���, �����)
			void updatePlot(const LegendItemList &legendItems);

			/// ��� ������
			void setLegendType(const LegendType &legendType);

			/// ������ ����������
			void histPrint();

			/// ��������� ����������� � ����
			void histSaveToFile(const QString& fileName);

		private:
			/// �������� ����� ��� Y
			void updateYAxis();

			/// ����� ���� ����
			void resetAxis();

			QwtPlotMultiBarChart* barChartPlot_;
			LegendItemList legendItems_;        //!< �������� �������
			QwtPlot* plot_;                     
			QwtPlotGrid* grid_;                 //!< �����
			CountAxis * scaleLeft_;             //!< ����� ����� ��� Y (�������������� ��� �����������)
			CountAxis * scaleRight_;            //!< ����� ������ ��� Y (�������������� ��� �����������)
			TimeAxis* timeScaleBottom_;         //!< ����� ������ ��� X (������� �������)
			TimeAxis* timeScaleTop_;            //!< ����� ������� ��� X (������� �������)    
			NpsZoneItem* timeZone_;             //!< ��������� ���������� ���������
			TimePicker* bottomTimePicker_;		//!< ������ � ������ ����� ������� 
			TimePicker* topTimePicker_;			//!< ������ � ������� ����� ������� 

			QwtPlotZoomer *zoomerTopRight_;
			QwtPlotZoomer *zoomerBottomLeft_;
			double yAxisMaxValue_;				//!< ������������ �������� �� ��� Y
			QMap<NpsZoneItem*, QList<QwtPlotMarker*>> npsMarkerMap_;
			PlansPicker *plansPiker_;
			const int histItemsSpacing_ = 15;		//!< ���������� ����� �������� ��������� ����������
			//LegendType legendType_;
		};
	}
}