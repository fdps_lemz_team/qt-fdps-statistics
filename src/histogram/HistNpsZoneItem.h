#pragma once

#include <QtGui/QColor>
#include <QtCore/QString>

#include <qwt_plot_zoneitem.h>

namespace Fdps {
    namespace statistics {

            /// \brief ����� ��� ����������� ������� ���������� �����������.
            class NpsZoneItem : public QwtPlotZoneItem {
            public:
                NpsZoneItem(const QString &title = "") {
                    //setTitle(title);
                    setZ(4);
                    setOrientation(Qt::Horizontal);

					QwtText curTimeMarkerText = QwtText(title);
					curTimeMarkerText.setFont(QFont("Courier New", 10, QFont::DemiBold));
					setTitle(curTimeMarkerText);
					
                }

                /// ������ ���� ������� � �������
                void setColor(const QColor &color) {
                    QColor c = color;

                    c.setAlpha(100);
                    setPen(c, 3.0);

                    c.setAlpha(40);
                    setBrush(c);
                }
            };

        } // namespace statistics
} // namespace Fdps