#pragma once

#include <QtCore/QTime>

#include <qwt_date_scale_draw.h>

namespace Fdps {
    namespace statistics {

            /// ����� ��� ��������� ����� ��� ��� X (����� ������ � ���������� 1 ���).
            class TimeAxis : public QwtScaleDraw {
            public:
                TimeAxis(Qt::Alignment labelAlign) {
                    setLabelAlignment(labelAlign);
                }

                /// ����� ����� ������� ����� � ����������� �� ��������
                virtual QwtText label(double v) const {
					if (v < 0 || v > 23) {
						return QwtText("");
					}

                    QTime upTime = QTime(0, 0, 0).addSecs((int)v * 3600);
                    QwtText retValue(upTime.toString("hh"));
                    retValue.setFont(QFont("Courier New", 8));
                    return retValue;
                }
            };

        } // namespace statistics
} // namespace Fdps