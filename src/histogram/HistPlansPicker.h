#pragma once

#include <QtCore/QObject>
#include <QtCore/QString>
#include <QtCore/QTime>
#include <QtWidgets/QWidget>

#include <qwt_plot_picker.h>

#include <LegendItem.h>

namespace Fdps {
    namespace statistics {

            /// класс реализует формуляр, возникающий в области графика при движении мыши.
            /// при наведении на элемент статистики планов, отображается время начала/ окончания действия ограничения, название элемента.
            class PlansPicker : public QwtPlotPicker {
                Q_OBJECT

            public:
                PlansPicker(int xAxis, int yAxis,
                    QwtPicker::RubberBand rubberBand, QwtPicker::DisplayMode trackerMode, QWidget * plot)
                    : QwtPlotPicker(xAxis, yAxis, rubberBand, trackerMode, plot)
                    , legendItems_(nullptr) {

					setTrackerPen(QColor(Qt::black));
					setTrackerFont(QFont("Courier New", 13, QFont::DemiBold));
                }


                /// задание списка с ограничениями
                void setLegendItems(LegendItemList * legendItems) {
					legendItems_ = legendItems;
                }


				/// промежутки между элементами (между разными часами)
				void setHistItemsSpacing(const int & itemsSpacing) {
					histItemsSpacing_ = itemsSpacing;
				}


				/// тип структуры
				void setLegendType(const LegendType & legendType) {
					legendType_ = legendType;
				}


                /// переопределение функции отображения формуляра
                virtual QwtText trackerText(const QPoint &pos) const {
                    QPointF curPnt = invTransform(pos);
					// отмеряем histItemsSpacing_ пискелей
					QPointF spacingPnt = invTransform(QPoint(pos.x() - histItemsSpacing_, 0));
					const double spacingTransformed = curPnt.x() - spacingPnt.x();

                    QTime curTime = QTime(0, 0, 0).addSecs(int(curPnt.x() * 3600.0));
					int curHour = curTime.minute() < 30 ? curTime.hour() : curTime.hour() + 1;
					QString curText = curHour < 10 ? QString("Время: 0%1") : QString("Время: %1");
					curText = curText.arg(curHour);

                    if (legendItems_ != nullptr) {
						const double minHourCoord = curHour - 0.5 + spacingTransformed / 2.0;
						const double maxHourCoord = curHour + 0.5 - spacingTransformed / 2.0;
						
						if (curPnt.x() < minHourCoord || curPnt.x() >maxHourCoord) {
							return QwtText(curText, QwtText::PlainText);
						}
												
						const double histItemWidht = ( maxHourCoord - minHourCoord ) / legendItems_->size();
						
						const int itemNum = int(( curPnt.x() - minHourCoord ) / histItemWidht);
												
						if (itemNum >= 0 && itemNum < legendItems_->size()) {
							if (legendItems_->at(itemNum).histData.size() > curHour) {
								
								if (curPnt.y() <= legendItems_->at(itemNum).histData.at(curHour)) {
									curText += "\n" + legendTypeToString(legendType_) + ": " + legendItems_->at(itemNum).name + 
										"\nКол-во: " + QString::number(legendItems_->at(itemNum).histData.at(curHour));
								}
							}
						}						
                    }

                    return QwtText(curText, QwtText::PlainText);
                }

            private:
                LegendItemList * legendItems_;
                QString label_;
				int histItemsSpacing_;	//!< промежутки между группами элементов гистограмм
				LegendType legendType_;
            };

        } // namespace statistics
} // namespace Fdps