#pragma once

#include <qwt_picker.h>
#include <qwt_plot_zoomer.h>


namespace Fdps {
	namespace statistics {

		class Zoomer : public QwtPlotZoomer {
		public:
			Zoomer(int xAxis, int yAxis, QWidget *canvas) :
				QwtPlotZoomer(xAxis, yAxis, canvas) {

				setResizeMode(QwtPicker::KeepSize);
				setRubberBand(QwtPicker::RectRubberBand);
				setRubberBandPen(QColor(Qt::green));
				setTrackerMode(QwtPicker::AlwaysOff);
				//setTrackerPen(QColor(Qt::white));
				setMaxStackDepth(1);

				// RightButton: zoom out by 1
				// Ctrl+RightButton: zoom out to full size

				setMousePattern(QwtEventPattern::MouseSelect2,
					Qt::RightButton, Qt::ControlModifier);
				setMousePattern(QwtEventPattern::MouseSelect3,
					Qt::RightButton);
			}
		};
	}
}
