#pragma once

#include <qwt_scale_draw.h>

//#include <RvmInfo.h>

namespace Fdps {
    namespace statistics {

            /// ����� ��� ��������� ����� ��� ��� Y (�������� �����������).  
            class CountAxis : public QwtScaleDraw {
            public:
                CountAxis(Qt::Alignment labelAlign) {
                    setTickLength(QwtScaleDiv::MinorTick, 0);
                    setTickLength(QwtScaleDiv::MediumTick, 0);
                    setTickLength(QwtScaleDiv::MajorTick, 3);

                    setLabelAlignment(labelAlign);
                }                
            };

        } // namespace statistics
} // namespace Fdps