#pragma once

#include <QtGui/QColor>
#include <QtCore/QString>

#include <qwt_plot_zoneitem.h>

namespace Fdps {
    namespace statistics {

            /*!
              \brief ����� ��� ����������� ������� �� ������� ���� �����������.
              ������ �������������� �������, ����������� ���� �����������.
            */
            class RvmZoneItem : public QwtPlotZoneItem {
            public:
                RvmZoneItem(const QString &title) {
                    setTitle(title);
                    setZ(1500); // on top the the grid
                    setOrientation(Qt::Horizontal);
                    setItemAttribute(QwtPlotItem::Legend, false);
                }

                /// ������ ���� ������� � �������
                void setColor(const QColor &color) {
                    QColor c = color;

                    c.setAlpha(100);
                    //setPen(c);
                    setPen(Qt::red);

                    c.setAlpha(20);
                    setBrush(c);
                }

                /// ������ �������� ����
                void setInterval(const double &i1, const double &i2) {
                    QwtPlotZoneItem::setInterval(i1, i2);
                }
            };

        } // namespace statistics
} // namespace Fdps