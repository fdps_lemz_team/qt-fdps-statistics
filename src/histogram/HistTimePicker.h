#pragma once

#include <QtGui/QColor>
#include <QtCore/QObject>
#include <QtCore/QString>
#include <QtCore/QTime>
#include <QtGui/qevent.h>
#include <QtWidgets/QWidget>

#include <qwt_plot.h>
#include <qwt_plot_picker.h>
#include <qwt_plot_zoneitem.h>
#include <qwt_picker_machine.h>


namespace Fdps {
    namespace statistics {

            /// класс реализует формуляр, возникающий в области графика при движении мыши.
            /// при наведении на элемент ограничений, отображается время начала/ окончания действия ограничения.
            class TimePicker : public QwtPlotPicker {
                Q_OBJECT
            public:
				TimePicker(int xAxis, int yAxis,
					QwtPicker::RubberBand rubberBand, QwtPicker::DisplayMode trackerMode, QWidget * plot)
					: QwtPlotPicker(xAxis, yAxis, rubberBand, trackerMode, plot)
					, timeZoneItem_(new QwtPlotZoneItem()) {

					setStateMachine(new QwtPickerTrackerMachine());
					setRubberBandPen(QPen(QColor(Qt::green), 3.0));					
					setTrackerPen(QColor(Qt::black));
					setTrackerFont(QFont("Courier New", 10, QFont::DemiBold));


					timeZoneItem_->setOrientation(Qt::Vertical);

					QColor c = QColor("#282CFF");
					c.setAlpha(100);
					timeZoneItem_->setPen(c, 3.0);

					c.setAlpha(20);
					timeZoneItem_->setBrush(c);

					timeZoneItem_->setVisible(false);
					timeZoneItem_->attach(this->plot());
				}
				

                /// переопределение функции отображения формуляра
                virtual QwtText trackerText(const QPoint &pos) const {
					
					const int curHour = int(invTransform(pos).x()) + 1;
                    return QwtText(curHour < 10 ? QString("0%1").arg(curHour) : QString::number(curHour));
                }

				
				void widgetMousePressEvent(QMouseEvent * ev) {
					const int curHour = int(invTransform(ev->pos()).x()) + 1;

					timeZoneItem_->setInterval(curHour - 0.5, curHour + 0.5);
					timeZoneItem_->setVisible(true);
				}


				void widgetMouseReleaseEvent(QMouseEvent * ev) {	
					Q_UNUSED(ev);
					timeZoneItem_->setVisible(false);
				}


				void widgetMouseDoubleClickEvent(QMouseEvent * ev) {
					const int curHour = int(invTransform(ev->pos()).x()) + 1;

					plot()->setAxisScale(QwtPlot::xBottom, curHour - 0.5, curHour + 0.5, 1);

					plot()->setAxisScale(QwtPlot::xTop, curHour - 0.5, curHour + 0.5, 1);
				}

            private:
                QString label_;
				QwtPlotZoneItem * timeZoneItem_;
            };

    } // namespace statistics
} // namespace Fdps