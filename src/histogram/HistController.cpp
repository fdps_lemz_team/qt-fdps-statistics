#include "HistController.h"

#include <QtPrintSupport/QPrinter>
#include <QtPrintSupport/QPrintDialog>

#include <qwt_date_scale_engine.h>

#include <qwt_column_symbol.h>

#include <qwt_legend.h>

#include <qwt_plot_canvas.h>
#include <qwt_plot_grid.h>
#include <qwt_plot_layout.h>
#include <qwt_plot_magnifier.h>
#include <qwt_plot_marker.h>
#include <qwt_plot_multi_barchart.h>
#include <qwt_plot_renderer.h>
#include <qwt_plot_scaleitem.h>
#include <qwt_plot_textlabel.h>
#include <qwt_plot_zoomer.h>

#include <qwt_scale_widget.h>

#include <qwt_symbol.h>


#include <HistCountAxis.h>
#include <HistNpsZoneItem.h>
#include <HistPlansItem.h>
#include <HistPlansPicker.h>
#include <HistTimeAxis.h>
#include <HistTimePicker.h>


namespace Fdps {
	namespace statistics {

		const QColor warnColor = QColor("#ff7070");
		const QColor normColor = QColor("#00aa00");

		HistController::HistController(QWidget* parent)
			:QObject(parent)
			, plot_(new QwtPlot(parent))
			, grid_(new QwtPlotGrid())
			, scaleLeft_(new CountAxis(Qt::AlignLeft))
			, scaleRight_(new CountAxis(Qt::AlignRight))
			, timeScaleBottom_(new TimeAxis(Qt::AlignBottom))
			, timeScaleTop_(new TimeAxis(Qt::AlignTop))
			, timeZone_(new NpsZoneItem())
			, yAxisMaxValue_(0.0) {
				
			plot_->setPalette(parent->palette().background().color());
			plot_->canvas()->setPalette(parent->palette().background().color());
			plot_->enableAxis(QwtPlot::xBottom, true);
			plot_->enableAxis(QwtPlot::xTop, true);
			plot_->enableAxis(QwtPlot::yLeft, false);
			plot_->enableAxis(QwtPlot::yRight, false);
			plot_->setAutoReplot(true);

			resetAxis();

			
			
			barChartPlot_ = new QwtPlotMultiBarChart();
			barChartPlot_->setLayoutPolicy(QwtPlotMultiBarChart::AutoAdjustSamples);
			barChartPlot_->setSpacing(histItemsSpacing_);
			barChartPlot_->setMargin(15);
			barChartPlot_->attach(plot_);
			barChartPlot_->setStyle(QwtPlotMultiBarChart::Grouped);
			barChartPlot_->setOrientation(Qt::Vertical);
			

			

			// �����
			grid_->enableX(true);
			grid_->enableY(true);
			grid_->setZ(3);
			grid_->setPen(QColor("#4756FF"), 1.0, Qt::DashLine);
			grid_->attach(plot_);	
						


			// �����������
			zoomerBottomLeft_ = new Zoomer(QwtPlot::xBottom, QwtPlot::yLeft, plot_->canvas());
			zoomerTopRight_ = new Zoomer(QwtPlot::xTop, QwtPlot::yRight, plot_->canvas());
			
			connect(zoomerBottomLeft_, &QwtPlotZoomer::zoomed, this, [&] () {
				if (zoomerBottomLeft_->zoomRectIndex() == 0) {
					resetAxis();
				}
			});


			// ������ ������� �������
			bottomTimePicker_ = new TimePicker(QwtPlot::xBottom, QwtPlot::yLeft, QwtPlotPicker::VLineRubberBand, 
				QwtPicker::AlwaysOn, plot_->axisWidget(QwtPlot::xBottom));

			topTimePicker_ = new TimePicker(QwtPlot::xTop, QwtPlot::yLeft, QwtPlotPicker::VLineRubberBand, 
				QwtPicker::AlwaysOn, plot_->axisWidget(QwtPlot::xTop));

			// ������ �������� �����������
			plansPiker_ = new PlansPicker(QwtPlot::xBottom, QwtPlot::yLeft, QwtPlotPicker::CrossRubberBand,
				QwtPicker::AlwaysOn, plot_->canvas());
			plansPiker_->setHistItemsSpacing(histItemsSpacing_);
		}

		
		QWidget * HistController::getHistWidget() const {
			return plot_;
		}


		void HistController::setGridVisible(bool visible) {
			grid_->setVisible(visible);
		}

				
		void HistController::updatePlot(const LegendItemList &legendItems) {
			legendItems_ = legendItems;

			plansPiker_->setLegendItems(&legendItems_);
			
			for (auto key : npsMarkerMap_.keys()) {
				for (auto markerIt : npsMarkerMap_.take(key)) {
					markerIt->detach();
					delete markerIt;
				}
				key->detach();
				delete key;
			}

			for (auto it : legendItems_) {
				if (it.npsChecked) {
					NpsZoneItem * curNpsMarker = new NpsZoneItem(QString("��� ��� %1").arg(it.name));
					if (it.curMaxPs > it.nps) {
						curNpsMarker->setInterval(it.nps, it.curMaxPs);
						curNpsMarker->setColor(warnColor);
					} else {
						curNpsMarker->setInterval(it.nps, it.nps);
						curNpsMarker->setColor(normColor);
					}
					curNpsMarker->attach(plot_);		
				
					QList<QwtPlotMarker*> markers;

					for (int nn = 0; nn < it.curPs.size(); ++nn) {
						QwtPlotMarker * curMarker = new QwtPlotMarker();
						curMarker->setValue(QPointF(nn, it.curMaxPs > it.nps ?  it.curMaxPs + 1.0 : it.nps + 1.0));

						QwtText curText = QwtText(it.curPs.at(nn) > it.nps ? 
							QString("+%1").arg(it.curPs.at(nn) - it.nps)
							: QString("-%1").arg(it.nps - it.curPs.at(nn)));

						curText.setFont(QFont("Courier New", 14, QFont::DemiBold));
						curText.setColor(it.curPs.at(nn) > it.nps ? warnColor : normColor);

						curMarker->setLabel(curText);
						curMarker->setZ(10);
						curMarker->attach(plot_);
						
						markers.append(curMarker);
					}


					npsMarkerMap_.insert(curNpsMarker, markers);
				}
			}
					   
			yAxisMaxValue_ = 0.0;
			for (auto it: legendItems_) {
				for (auto subIt : it.histData) {
					if (subIt > yAxisMaxValue_) {
						yAxisMaxValue_ = subIt;
					}
				}
			}
			
			updateYAxis();
			
			for (int i = 0; i < legendItems_.count(); ++i) {
				QwtColumnSymbol *symbol = new QwtColumnSymbol(QwtColumnSymbol::Box);
				symbol->setLineWidth(1);
				symbol->setFrameStyle(QwtColumnSymbol::Raised);
				symbol->setPalette(QPalette(QColor(legendItems_[i].color)));
				symbol->setStyle(QwtColumnSymbol::Box);

				barChartPlot_->setSymbol(i, symbol);
			}

			QVector<QVector<double>> histData;
			for (int nn = 0; nn < 24; ++nn) {
				QVector<double> hourData;
				for (auto legendIt : legendItems_) {
					if (legendIt.histData.size() > nn) {
						hourData << double(legendIt.histData.at(nn));
					}
				}
				histData.append(hourData);
			}
			
			barChartPlot_->setSamples(histData);
			barChartPlot_->setZ(1);
		}


		void HistController::setLegendType(const LegendType &legendType) {
			plansPiker_->setLegendType(legendType);
		}


		void HistController::histPrint() {
			QPrinter printer(QPrinter::HighResolution);

			QString docName = plot_->title().text();
			if (!docName.isEmpty()) {
				docName.replace(QRegExp(QString::fromLatin1("\n")), tr(" -- "));
				printer.setDocName(docName);
			}

			printer.setCreator("Bode example");
			printer.setOrientation(QPrinter::Landscape);

			QPrintDialog dialog(&printer);
			if (dialog.exec()) {
				QwtPlotRenderer renderer;

				if (printer.colorMode() == QPrinter::GrayScale) {
					renderer.setDiscardFlag(QwtPlotRenderer::DiscardBackground);
					renderer.setDiscardFlag(QwtPlotRenderer::DiscardCanvasBackground);
					renderer.setDiscardFlag(QwtPlotRenderer::DiscardCanvasFrame);
					renderer.setLayoutFlag(QwtPlotRenderer::FrameWithScales);
				}

				renderer.renderTo(plot_, printer);
			}
		}


		void HistController::histSaveToFile(const QString& fileName) {
			QwtPlotRenderer renderer;
			renderer.exportTo(plot_, QString("%1.pdf").arg(fileName));
		}


		void HistController::updateYAxis() {
			plot_->enableAxis(QwtPlot::yLeft, true);
			plot_->setAxisScale(QwtPlot::yLeft, -1, yAxisMaxValue_ + 5, 5);
			plot_->setAxisScaleDraw(QwtPlot::yLeft, scaleLeft_);

			plot_->enableAxis(QwtPlot::yRight, true);
			plot_->setAxisScale(QwtPlot::yRight, -1, yAxisMaxValue_ + 5, 5);
			plot_->setAxisScaleDraw(QwtPlot::yRight, scaleRight_);
		}


		void HistController::resetAxis() {
			plot_->setAxisScaleDraw(QwtPlot::xBottom, timeScaleBottom_);
			plot_->setAxisScale(QwtPlot::xBottom, -1, 24, 1);

			plot_->setAxisScaleDraw(QwtPlot::xTop, timeScaleTop_);
			plot_->setAxisScale(QwtPlot::xTop, -1, 24, 1);

			updateYAxis();
		}
	}
}