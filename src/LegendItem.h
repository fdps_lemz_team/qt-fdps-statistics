#pragma once

#include <QtCore/QDataStream>
#include <QtCore/QJsonArray>
#include <QtCore/QJsonObject>
#include <QtCore/QMap>
#include <QtCore/QMetaObject>
#include <QtCore/QString>
#include <QtCore/QVector>

namespace Fdps {
	namespace statistics {

		enum LegendType {
			LegendRc,
			LegendAirport,
			LegendAirway,
			LegendPoint,
			LegendUnknwn,
		};


		inline QString legendTypeToString(const LegendType &legendType) {
			switch (legendType) {
			case	LegendRc:
				return "Сектор";
			case	LegendAirport:
				return "Аэродром";
			case	LegendAirway:
				return "Трасса";
			case	LegendPoint:
				return "Точка";
			case	LegendUnknwn:
				return "";
			}
			return "";
		}


		namespace json_defs {
			const QString colorKey = "Color";
			const QString nameKey = "Name";
			const QString checkedKey = "Checked";
			const QString npsKey = "Nps";
			const QString checkNpsKey = "NpsChecked";
		}

		//const QString defColor = "#99FF74";
		const QStringList defColors = QStringList{ 
			"#F6EAB8", "#E15B26", "#CEEBFB", "#2BAE8C", "#7F706B", "#EACFDE", "#FEE1E5", "#7EAEB0",
			"#FCE691", "#DA2B30", "#BBE3EB", "#2C934A", "#D5A87E", "#D8B8C3", "#F3BDBB", "#6EBE45", 
			"#FFD833", "#9C2936", "#93B6D2", "#B5D781", "#D28829", "#B98ABE", "#F9BFD8", "#009A82",
			"#FDC031", "#8E1E59", "#14B9B5", "#669D40", "#CB6729", "#ACA5D1", "#F08BB7", "#6A7E36",
			"#FBBA6A", "#F89985", "#32C1D1", "#366833", "#E1AD35", "#6F73A2", "#F27092", "#DDE7B2",
			"#F79240", "#E25A66", "#6E92A2", "#A09E37", "#8A5D3C", "#D1D4E5", "#E9A9BA", "#B4C07E",
			"#F69A67", "#F36F6A", "#027BA8", "#C1D331", "#5C3816", "#8D4585", "#CC97B5", "#C4D0BA",
			"#FDDBAB", "#E68189", "#667AB9", "#98BF3E", "#9B543E", "#793292", "#E270AC", "#DDE0CD",
			"#F8B097", "#D51B6F", "#254262", "#B6C38E", "#BF9170", "#5C276D", "#CC3793", "#8C9C78" };
		static int curDefColorNum = 1;
		const QString defName = "???";
		const int defNps = -1;	


		/// предоставить цвет по умолчанию
		inline QString getDefColor() {
			if (++curDefColorNum >= defColors.size()) {
				curDefColorNum = 0;
			}

			return defColors.at(curDefColorNum);
		}


		/// параметры элемента в легенде элемента
		struct LegendItem {
			QString color;				//!< цвет гисторграммы
			QString name;				//!< название сектора/точки/трассы/аэродрома
			bool isChecked;				//!< признак отметки в легенде
			bool npsChecked;			//!< признак отметки в легенде
			int nps;					//!< заданная норма пропускной способности
			QList<int> curPs;			//!< пропускная способность для каждого часа
			int curMaxPs;				//!< максимальное значение пропускной способности
			int histTotal;				//!< всего за сутки
			QVector<double> histData;	//!< значения для элемента гистограммы по каждому часу


			LegendItem(const QString& aColor = "", const QString& aName = defName, const bool& aIsChecked = false,
				const bool& aNpsChecked = false, const int& aMaxNps = defNps, const QList<int>& aCurNps = QList<int>(),
				const int& aCurMaxPs = 0) {
				color = aColor.isEmpty() ? getDefColor() : aColor;
				name = aName;
				isChecked = aIsChecked;
				npsChecked = aNpsChecked;
				nps = aMaxNps;
				curPs = aCurNps;
				curMaxPs = aCurMaxPs;
			}

			friend bool operator==(const LegendItem& left, const LegendItem& right) {
				return (
					left.color == right.color &&
					left.name == right.name &&
					left.isChecked == right.isChecked &&
					left.npsChecked == right.npsChecked &&
					left.nps == right.nps &&
					left.curPs == right.curPs &&
					left.curMaxPs == right.curMaxPs);
			}

			friend bool operator!=(const LegendItem& left, const LegendItem& right) {
				return !(left == right);
			}

			/// преобразование настроек в Json объект.
			QJsonObject toJsonObject() const {
				QJsonObject retValue;
				retValue.insert(json_defs::colorKey, QJsonValue(color));
				retValue.insert(json_defs::nameKey, QJsonValue(name));
				retValue.insert(json_defs::checkedKey, QJsonValue(isChecked));
				retValue.insert(json_defs::checkNpsKey, QJsonValue(npsChecked));
				retValue.insert(json_defs::npsKey, QJsonValue(nps));
				return retValue;
			}


			/// восстановление настроек из Json объекта.
			static LegendItem fromJsonObject(const QJsonObject& jsonObj) {
				LegendItem retValue;
				retValue.color = jsonObj.value(json_defs::colorKey).toString(getDefColor());
				retValue.name = jsonObj.value(json_defs::nameKey).toString(defName);
				retValue.isChecked = jsonObj.value(json_defs::checkedKey).toBool(false);
				retValue.npsChecked = jsonObj.value(json_defs::checkNpsKey).toBool(false);
				retValue.nps = jsonObj.value(json_defs::npsKey).toInt(defNps);
				return retValue;
			}
		};


		struct LegendItemList : public QList<LegendItem> {

			QJsonArray toJsonArray() const {
				QJsonArray retValue;
				for (auto it : *this) {
					retValue.append(it.toJsonObject());
				}
				return retValue;
			}

			static LegendItemList fromJsonArray(const QJsonArray& jsonArr) {
				LegendItemList retValue;
				for (auto jsonObgIt : jsonArr) {
					retValue.append(LegendItem::fromJsonObject(jsonObgIt.toObject()));
				}
				return retValue;
			}

			LegendItem getLegendForName(const QString& name) {
				for (auto it : *this) {
					if (it.name == name) {
						return it;
					}
				}
				return LegendItem(getDefColor(), name, true);
			}

			void setLegendItem(const LegendItem& item) {
				for (auto it = begin(); it != end(); ++it) {
					if (it->name == item.name) {
						it->color = item.color;
						it->isChecked = item.isChecked;
						it->npsChecked = item.npsChecked;
						it->nps = item.nps;
						it->curPs = item.curPs;
						for (auto psIt : it->curPs) {
							if (psIt > it->curMaxPs) {
								it->curMaxPs = psIt;
							}
						}
						//it->histData = item.histData;
						return;
					}
				}
				this->append(item);
			}


			/// задание данных, полученных от сервиса
			void setRawData(const QString &name, const QMap<int, int> &rawData) {
				
				for (auto & elemIt : *this) {
					if (elemIt.name == name) {
						elemIt.histData.clear();
						elemIt.histTotal = 0;
						for (auto it : rawData) {
							elemIt.histData.append(double(it));
							elemIt.histTotal += it;
						}
					}
				}
			}
		};
	}
}
Q_DECLARE_METATYPE(Fdps::statistics::LegendItem)
