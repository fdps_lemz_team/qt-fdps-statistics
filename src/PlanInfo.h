#pragma once 

#include <QtCore/QDateTime>
#include <QtGui/QColor>
#include <QtCore/QJsonArray>
#include <QtCore/QJsonDocument>
#include <QtCore/QJsonObject>
#include <QtCore/QString>
#include <QtCore/QVector>

namespace Fdps {
	namespace statistics {

		const QString DailyCountKey = "Daily";
		const QString PerformCountKey = "Perform";
		const QString CancelCountKey = "Cancel";
		const QString UnperformCountKey = "Unperform";
	
		const QString ArrCountKey = "Arr";
		const QString DepCountKey = "Dep";
		const QString TransitCountKey = "Transit";
		const QString TotalCountKey = "Total";
	
		const QString RcPlansKey = "Rc";
		const QString PointPlansKey = "Point";
		const QString AirdromPlansKey = "Airdrom";
		const QString AirwayPlansKey = "Airway";


		/// �� ���� ��������� �����
		struct PlanProcType {
			QMap<int, int> dailyCount;
			QMap<int, int> performCount;
			QMap<int, int> cancelCount;
			QMap<int, int> unperformCount;

			friend bool operator==(const PlanProcType& left, const PlanProcType& right) {
				return (left.dailyCount == right.dailyCount &&
					left.performCount == right.performCount &&
					left.cancelCount == right.cancelCount &&
					left.unperformCount == right.unperformCount
					);
			}
			
			friend bool operator!=(const PlanProcType& left, const PlanProcType& right) {
				return !(left == right);
			}
			
			static PlanProcType fromJsonObject(const QJsonObject& jsonObj) {
				PlanProcType retValue;

				if (jsonObj.contains(DailyCountKey) && jsonObj.value(DailyCountKey).isObject()) {
					auto dailyObj = jsonObj.value(DailyCountKey).toObject();
					for (auto key : dailyObj.keys()) {
						bool ok;
						int hour = key.toInt(&ok, 10);
						retValue.dailyCount[hour] = dailyObj.value(key).toInt();
					}
				}			

				if (jsonObj.contains(PerformCountKey) && jsonObj.value(PerformCountKey).isObject()) {
					auto performObj = jsonObj.value(PerformCountKey).toObject();
					for (auto key : performObj.keys()) {
						bool ok;
						int hour = key.toInt(&ok, 10);
						retValue.performCount[hour] = performObj.value(key).toInt();
					}
				}

				if (jsonObj.contains(CancelCountKey) && jsonObj.value(CancelCountKey).isObject()) {
					auto cancelObj = jsonObj.value(CancelCountKey).toObject();
					for (auto key : cancelObj.keys()) {
						bool ok;
						int hour = key.toInt(&ok, 10);
						retValue.cancelCount[hour] = cancelObj.value(key).toInt();
					}
				}

				if (jsonObj.contains(UnperformCountKey) && jsonObj.value(UnperformCountKey).isObject()) {
					auto unperformObj = jsonObj.value(UnperformCountKey).toObject();
					for (auto key : unperformObj.keys()) {
						bool ok;
						int hour = key.toInt(&ok, 10);
						retValue.unperformCount[hour] = unperformObj.value(key).toInt();
					}
				}

				return retValue;
			}
		};

		
		/// ��������� �� �������
		struct Rc {
			PlanProcType arrCount;
			PlanProcType depCount;
			PlanProcType transitCount;
			PlanProcType totalCount;
			
			friend bool operator==(const Rc& left, const Rc& right) {
				return (left.arrCount == right.arrCount &&
					left.depCount == right.depCount &&
					left.transitCount == right.transitCount &&
					left.totalCount == right.totalCount
					);
			}
			
			friend bool operator!=(const Rc& left, const Rc& right) {
				return !(left == right);
			}
			
			static Rc fromJsonObject(const QJsonObject& jsonObj) {
				Rc retValue;
				
				if (jsonObj.contains(ArrCountKey) && jsonObj.value(ArrCountKey).isObject()) {
					retValue.arrCount = PlanProcType::fromJsonObject(jsonObj.value(ArrCountKey).toObject());
				}

				if (jsonObj.contains(DepCountKey) && jsonObj.value(DepCountKey).isObject()) {
					retValue.depCount = PlanProcType::fromJsonObject(jsonObj.value(DepCountKey).toObject());
				}

				if (jsonObj.contains(TransitCountKey) && jsonObj.value(TransitCountKey).isObject()) {
					retValue.transitCount = PlanProcType::fromJsonObject(jsonObj.value(TransitCountKey).toObject());
				}

				if (jsonObj.contains(TotalCountKey) && jsonObj.value(TotalCountKey).isObject()) {
					retValue.totalCount = PlanProcType::fromJsonObject(jsonObj.value(TotalCountKey).toObject());
				}

				return retValue;
			}
		};

		

		/// ��������� �� ���������
		struct Airport {
			PlanProcType arrCount;
			PlanProcType depCount;
			PlanProcType totalCount;
						
			friend bool operator==(const Airport& left, const Airport& right) {
				return (left.arrCount == right.arrCount &&
					left.depCount == right.depCount &&
					left.totalCount == right.totalCount
					);
			}
			
			friend bool operator!=(const Airport& left, const Airport& right) {
				return !(left == right);
			}
			
			static Airport fromJsonObject(const QJsonObject& jsonObj) {
				Airport retValue;
				retValue.arrCount = PlanProcType::fromJsonObject(jsonObj.value(ArrCountKey).toObject());
				retValue.depCount = PlanProcType::fromJsonObject(jsonObj.value(DepCountKey).toObject());
				retValue.totalCount = PlanProcType::fromJsonObject(jsonObj.value(TotalCountKey).toObject());

				return retValue;
			}
		};

		struct Airway {
			PlanProcType arrCount;
			PlanProcType depCount;
			PlanProcType transitCount;
			PlanProcType totalCount;

			friend bool operator==(const Airway& left, const Airway& right) {
				return ( left.arrCount == right.arrCount &&
					left.depCount == right.depCount &&
					left.transitCount == right.transitCount &&
					left.totalCount == right.totalCount
					);
			}

			friend bool operator!=(const Airway& left, const Airway& right) {
				return !( left == right );
			}

			static Airway fromJsonObject(const QJsonObject& jsonObj) {
				Airway retValue;

				if (jsonObj.contains(ArrCountKey) && jsonObj.value(ArrCountKey).isObject()) {
					retValue.arrCount = PlanProcType::fromJsonObject(jsonObj.value(ArrCountKey).toObject());
				}

				if (jsonObj.contains(DepCountKey) && jsonObj.value(DepCountKey).isObject()) {
					retValue.depCount = PlanProcType::fromJsonObject(jsonObj.value(DepCountKey).toObject());
				}

				if (jsonObj.contains(TransitCountKey) && jsonObj.value(TransitCountKey).isObject()) {
					retValue.transitCount = PlanProcType::fromJsonObject(jsonObj.value(TransitCountKey).toObject());
				}

				if (jsonObj.contains(TotalCountKey) && jsonObj.value(TotalCountKey).isObject()) {
					retValue.totalCount = PlanProcType::fromJsonObject(jsonObj.value(TotalCountKey).toObject());
				}

				return retValue;
			}
		};


		/// ����� ���������� �� �����
		struct PlansInfo {
			QMap<QString, Rc> rcPlans;				//!< ������ �� �������� 
			QMap<QString, PlanProcType> pointPlans;	//!< ������ �� ������
			QMap<QString, Airport> airdromPlans;	//!< ������ �� ����������
			QMap<QString, Airway> airwayPlans;		//!< ������ �� �������
			
			friend bool operator==(const PlansInfo& left, const PlansInfo& right) {
				return (left.rcPlans == right.rcPlans &&
					left.pointPlans == right.pointPlans &&
					left.airdromPlans == right.airdromPlans &&
					left.airwayPlans == right.airwayPlans
					);
			}

			friend bool operator!=(const PlansInfo& left, const PlansInfo& right) {
				return !(left == right);
			}

			static PlansInfo fromJsonObject(const QJsonObject& jsonObj) {
				PlansInfo retValue;
				
				if (jsonObj.contains(RcPlansKey) && jsonObj.value(RcPlansKey).isObject()) {
					auto rcObj = jsonObj.value(RcPlansKey).toObject();
					for (auto key : rcObj.keys()) {
						retValue.rcPlans[key] = Rc::fromJsonObject(rcObj.value(key).toObject());
					}
				}
			
				if (jsonObj.contains(PointPlansKey) && jsonObj.value(PointPlansKey).isObject()) {
					auto pntObj = jsonObj.value(PointPlansKey).toObject();
					for (auto key : pntObj.keys()) {
						auto planProcObj = 
						retValue.pointPlans[key] = PlanProcType::fromJsonObject(pntObj.value(key).toObject());
					}
				}

				if (jsonObj.contains(AirdromPlansKey) && jsonObj.value(AirdromPlansKey).isObject()) {
					auto ardObj = jsonObj.value(AirdromPlansKey).toObject();
					for (auto key : ardObj.keys()) {
						retValue.airdromPlans[key] = Airport::fromJsonObject(ardObj.value(key).toObject());
					}
				}

				if (jsonObj.contains(AirwayPlansKey) && jsonObj.value(AirwayPlansKey).isObject()) {
					auto arwObj = jsonObj.value(AirwayPlansKey).toObject();
					for (auto key : arwObj.keys()) {
						retValue.airwayPlans[key] = Airway::fromJsonObject(arwObj.value(key).toObject());
					}
				}

				return retValue;
			}
		};
	} // namespace statistics
} // namespace Fdps
