#pragma once

#include <QtCore/QRect>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDesktopWidget>
#include <QtWidgets/QWidget>

#include <QtCore/QDebug>

namespace qt_utils {

	/// ����������, ��� widget �� ����� �� ������ ������
	inline void CheckWidgetPosition(QWidget* wgt) {
		if (wgt->pos().x() < -10)
			wgt->move(1, wgt->pos().y());

		if (wgt->pos().y() < -10)
			wgt->move(wgt->pos().x(), 1);

		const int sn = QApplication::desktop()->screenNumber(wgt);

		if (sn == -1)
			wgt->move(1, 1);

		QRect screenRect = QApplication::desktop()->screenGeometry(sn);

		if (wgt->pos().x() + wgt->width() > screenRect.x() + screenRect.width() - 50) {
			wgt->move(screenRect.x() + screenRect.width() - wgt->width() - 50, wgt->pos().y());
		}

		if (wgt->pos().y() + wgt->height() > screenRect.y() + screenRect.height() - 50) {
			wgt->move(wgt->pos().x(), screenRect.y() + screenRect.height() - wgt->height() - 50);
		}
	}
}
