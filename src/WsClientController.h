#pragma once

#include "WsClient.h"

namespace Fdps {
	namespace statistics {

		class WsClientController : public QObject {
			Q_OBJECT

		public:
			WsClientController(QObject* parent = 0);

			void startWork();

		public Q_SLOTS:
			/// �������� ������ �� WS
			void wsDataReceived(const QString& data);

			/// �������� ��������� ����������� WS �������
			void wsClientConnStateChanged(const bool & isConn);

			/// ���������� �������� ��������� �����������
			void needUpdateParams();

			/// ������� ���� ��� ������� ������
			void setDateForPlansRequest(const QDate &date);
			
		Q_SIGNALS:
			void wsSettingsUpdated(const WsClientParams&);
			/// ��������� ������.
			void wsSendTextMessage(const QByteArray& dataToSend);

			/// ��������� ������ � ���������� �����������
			void sendConnStateChanged(const QString &stateString);

			/// ��������� ������ � ������
			void sendPlansInfo(const PlansInfo & planInf);

		private:
			WsClient wsClient_;
			QDate curPlansDate_;	
		};
	}
}