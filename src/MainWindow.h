#pragma  once

#include <QtCore/QList>
#include <QtCore/QDateTime>
#include <QtCore/QSortFilterProxyModel>
#include <QtCore/QTimer>
#include <QtNetwork/QTcpServer>
#include <QtNetwork/QTcpSocket>
#include <QtWidgets/QMainWindow>

#include "LegendItem.h"
#include "LegendTableModel.h"
#include "HistController.h"
#include "PlansTableModel.h"
#include "SpinBoxDelegate.hpp"
#include "WsClientController.h"

#include "ui_MainWindow.h"


namespace Fdps {
	namespace statistics {

		class MainWindow : public QMainWindow {
			Q_OBJECT

		public:
			MainWindow(QWidget* parent = 0);
			~MainWindow();

		Q_SIGNALS:
			/// необхдимо обновить настройке подключения Ws
			void needUpdateWsSettings();

		private Q_SLOTS:
			/// применение настроек
			void applyBtnClicked();

			/// изменен тип структры
			void structCheckBoxToggled(bool toggled);

			/// изменен флаг "Выделить все"
			void selectAllCheckBoxToggled(bool toggled);
						
			/// 
			void legendTableClicked(const QModelIndex& index);

			///
			void legendTableDoubleClicked(const QModelIndex& index);

			///
			void itemChangedInLegend(const LegendItem& item);

			///
			void itemChangedInPlansTable(const LegendItem& item);

			/// печать таблицы
			void tablePrintBtnClicked();

			/// сохранить таблицу в файл
			void tableSaveToFileBtnClicked();

		private:
			/// инициализация вкладки "Настройки"
			void initSettingsTab();

			/// обновить доступность кнопок "Применить" и "Отмена"
			void settingsChanged();

			/// обновить параметры в GUI
			void updateStructData();

			/// сформировать данные для отрисовки гистограмм
			void formDataForHistogram();

			/// блокировка сигналов для группы "План"
			void blockSignalsForPlansGroup(bool block);

			/// блокировка сигналов для группы "Тип"
			void blockSignalsForTypeGroup(bool block);

			/// список легенды
			void updateLegend(const LegendItemList& items, const QString& title);

			/// тип структуры ВП
			LegendType curLegendType() const;

			/// выделенные элементы
			QStringList curCheckedNames() const;

			/// цвета элементов
			LegendItemList formLegendItems() const;

			/// обновить состояние флажка selectAll
			void updateCheckBoxSelectAll();

			/// заголовок для слъранения файла или печати
			QString formHtmlToFile();

			Ui::MainWindow gui_;
			WsClientController wsCntrl_;
			QSortFilterProxyModel plansProxyModel_;
			PlansTableModel plansSourceModel_;
			QIcon mainWindowIcon_;
			HistController* histCntrl_;
			PlansInfo plans_;
			LegendTableModel legendModel_;
			SpinBoxDelegate spinBoxDelegate_;
		};
	}
}
