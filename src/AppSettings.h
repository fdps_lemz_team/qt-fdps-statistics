#pragma once

#include <QtCore/QDataStream> 
#include <QtCore/QJsonObject>
#include <QtCore/QString>

#include "LegendItem.h"
#include "WsClientSettings.h"

namespace Fdps {
	namespace statistics {
		namespace json_defs {
			const QString gridKey = "ShowGrid";		
			const QString rcKey = "RcColor";
			const QString airportKey = "AirportColor";
			const QString airwayKey = "AirwayColor";
			const QString pointKey = "PointColor";			
		}


		///  цветовые настройки состояния станций.
		struct AppSettings {
			bool showGrid_;							//!< показывать сетку
			LegendItemList rcLegendSetts_;		//!< цветовые настройки рц
			LegendItemList airportLegendSetts_;	//!< цветовые настройки аэродромов
			LegendItemList airwayLegendSetts_;	//!< цветовые настройки трасс
			LegendItemList pointLegendSetts_;		//!< цветовые настройки точек
			WsClientParams wsSetts_;				//!< параметры подключения к сервису go
			

			friend bool operator==(const AppSettings& left, const AppSettings& right) {
				return (
					left.showGrid_				== right.showGrid_ &&
					left.rcLegendSetts_			== right.rcLegendSetts_ &&
					left.airportLegendSetts_	== right.airportLegendSetts_ &&
					left.airwayLegendSetts_		== right.airwayLegendSetts_&&
					left.pointLegendSetts_		== right.pointLegendSetts_ &&
					left.wsSetts_				== right.wsSetts_
					);
			}


			friend bool operator!=(const AppSettings& left, const AppSettings& right) {
				return !( left == right );
			}


			/// преобразование настроек в Json объект.
			QJsonObject toJsonObject() const {
				QJsonObject retValue;
				retValue.insert(json_defs::gridKey, QJsonValue(showGrid_));
				retValue.insert(json_defs::rcKey, QJsonValue(rcLegendSetts_.toJsonArray()));
				retValue.insert(json_defs::airportKey, QJsonValue(airportLegendSetts_.toJsonArray()));
				retValue.insert(json_defs::airwayKey, QJsonValue(airwayLegendSetts_.toJsonArray()));
				retValue.insert(json_defs::pointKey, QJsonValue(pointLegendSetts_.toJsonArray()));
				retValue.insert(json_defs::wsSettsKey, QJsonValue(wsSetts_.toJsonObject()));
				return retValue;
			}


			/// восстановление настроек из Json объекта.
			static AppSettings fromJsonObject(const QJsonObject& jsonObj) {
				AppSettings retValue;
				retValue.showGrid_				= jsonObj.value(json_defs::gridKey).toBool(false);
				retValue.rcLegendSetts_			= LegendItemList::fromJsonArray(jsonObj.value(json_defs::rcKey).toArray(QJsonArray()));
				retValue.airportLegendSetts_	= LegendItemList::fromJsonArray(jsonObj.value(json_defs::airportKey).toArray(QJsonArray()));
				retValue.airwayLegendSetts_		= LegendItemList::fromJsonArray(jsonObj.value(json_defs::airwayKey).toArray(QJsonArray()));
				retValue.pointLegendSetts_		= LegendItemList::fromJsonArray(jsonObj.value(json_defs::pointKey).toArray(QJsonArray()));
				retValue.wsSetts_				= WsClientParams::fromJsonObject(jsonObj.value(json_defs::wsSettsKey).toObject(QJsonObject()));
				return retValue;
			}


			LegendItemList getLegendItemsForNames(const QStringList & names, const LegendType & tp) {
				LegendItemList * resListPtr =nullptr;
				LegendItemList retValue;

				switch (tp) {
				case LegendRc:
					resListPtr = &rcLegendSetts_;
					break;
				case LegendAirport:
					resListPtr = &airportLegendSetts_;
					break;
				case LegendAirway:
					resListPtr = &airwayLegendSetts_;
					break;
				case LegendPoint:
					resListPtr = &pointLegendSetts_;
					break;
				}

				for (auto nameIt : names) {
					retValue.append(resListPtr->getLegendForName(nameIt));
				}

				return retValue;
			}

			QStringList getCheckedNames(const LegendType & tp) {
				LegendItemList * resListPtr =nullptr;
				QStringList retValue;

				switch (tp) {
				case LegendRc:
					resListPtr = &rcLegendSetts_;
					break;
				case LegendAirport:
					resListPtr = &airportLegendSetts_;
					break;
				case LegendAirway:
					resListPtr = &airwayLegendSetts_;
					break;
				case LegendPoint:
					resListPtr = &pointLegendSetts_;
					break;
				}

				for (auto it : *resListPtr) {
					if (it.isChecked) {
						retValue.append(it.name);
					}
				}
				 
				return retValue;
			}
							

			void updateLegendItem(const LegendItem& item, const LegendType& tp) {
				switch (tp) {
				case LegendRc:
					rcLegendSetts_.setLegendItem(item);
					break;
				case LegendAirport:
					airportLegendSetts_.setLegendItem(item);
					break;
				case LegendAirway:
					airwayLegendSetts_.setLegendItem(item);
					break;
				case LegendPoint:
					pointLegendSetts_.setLegendItem(item);
					break;
				}
			}
		};
	}
}
