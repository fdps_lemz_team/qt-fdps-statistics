#include "WsClientController.h"

#include "WsProtocol.h"


namespace Fdps {
	namespace statistics {

		WsClientController::WsClientController(QObject* parent /*= 0 */)
			: QObject(parent) {

			connect(&wsClient_, &WsClient::stateChaned, this, &WsClientController::wsClientConnStateChanged);
			connect(this, &WsClientController::wsSettingsUpdated, &wsClient_, &WsClient::updateParams);
			connect(&wsClient_, &WsClient::receiveData, this, &WsClientController::wsDataReceived);
			connect(this, &WsClientController::wsSendTextMessage, &wsClient_, &WsClient::sendTextMessage);
		}


		void WsClientController::wsDataReceived(const QString& data) {
			QJsonDocument   loadDoc(QJsonDocument::fromJson(data.toUtf8()));
			const QJsonObject& json = loadDoc.object();

			if (json.contains(json_defs::HeaderKey)) {

				if (json.value(json_defs::HeaderKey) == json_defs::AnswerPlansHdr) {
					if (json.contains(json_defs::PlansKey)) {

						Q_EMIT sendPlansInfo(PlansInfo::fromJsonObject(json[json_defs::PlansKey].toObject()));
					}
				}
			}
		}

				
		void WsClientController::startWork() {
			wsClient_.SetParams(SettsSingle::instance()->getAppSetts().wsSetts_);

			Q_EMIT sendConnStateChanged(QString("Подключение к серверу: Не подключено"));
		}


		void WsClientController::wsClientConnStateChanged(const bool &isConn) {
			Q_EMIT sendConnStateChanged(QString("Подключение к серверу: %1").arg(isConn ? "Подключено" : "Не подключено"));

			if (isConn) {
				//Q_EMIT wsSendTextMessage(CreateRequestAirportsMsg());

				if (curPlansDate_.isValid()) {
					Q_EMIT wsSendTextMessage(CreateRequestPlansMsg(curPlansDate_));
				}
			}
		}
		

		void WsClientController::needUpdateParams() {
			wsClient_.updateParams(SettsSingle::instance()->getAppSetts().wsSetts_);
		}


		void WsClientController::setDateForPlansRequest(const QDate &date) {
			curPlansDate_ = date;
			if (wsClient_.isConnected()) {
				Q_EMIT wsSendTextMessage(CreateRequestPlansMsg(curPlansDate_));
			}
		}
	}
}