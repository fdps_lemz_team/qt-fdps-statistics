#pragma once

#include <QCoreApplication>
#include <QDialog>
#include <QGraphicsView>
#include <QMainWindow>
#include <QScrollBar.h>
#include <QSettings>
#include <QSplitter>
#include <QString>
#include <QTableView>
#include <QVariant>
#include <QWidget>

namespace qt_utils {

    const QString posPostfix_ = "_Pos";                             //!< �������� ����� ���������.
    const QString sizePostfix_ = "_Size";                           //!< �������� ����� �������.
    const QString colWidthPostfix_ = "_Column_Width";               //!< �������� ����� ������ ������� �������.
    const QString splitterPostfix_ = "_Splitter_Width";             //!< �������� ������� �����������.
    const QString scrollHorizontalPostfix_ = "_Scroll_Horizontal";  //!< �������� ������� �������������� ���������.
    const QString scrollVerticalPostfix_ = "_Scroll_Vertical";      //!< �������� ������� ������������ ���������.
   

    /** ����� ������������ ��� ���������� � �������������� ���������� (������, ��������� �� ������,
    *   ������� �������� � ��������, ������� ������������ SplitLayout).
    */
    class WidgetParams {
    public:
        WidgetParams() {}

        /// ������ ���� � ����� � �����������.
        static void setFilePath(const QString& filePath) {
            filePath_ = filePath;
        }

        /** ��������� ��������� �������.
        *	\param [in] wgt ��������� �� �������������� ������.
        */
        static void save(QWidget* wgt) {
            checkFilePath();
            QSettings set(filePath_, QSettings::IniFormat);
            set.setIniCodec("UTF-8");

            // ��������� ������� � ������� ������� �������
            if (qobject_cast<QDialog*>(wgt) || qobject_cast<QMainWindow*>(wgt)) {
                set.setValue(wgt->objectName() + posPostfix_, QVariant(wgt->pos()));
                set.setValue(wgt->objectName() + sizePostfix_, QVariant(wgt->size()));
            }

            if (qobject_cast<QTableView*>(wgt)) {
                QList< int > sectionSizeList; // ������� ������ ������� ����� ��������� �����
                if (qobject_cast<QTableView*>(wgt)->model()) {
                    int col_count = qobject_cast<QTableView*>(wgt)->model()->columnCount(); //���������� ��� ������� �������� ���������� �������
                    for (int nn = 0; nn < col_count; ++nn) { // ������ ����� ���� ������� ��� ��������
						const int curWidht = qobject_cast< QTableView* >( wgt )->columnWidth(nn);
						sectionSizeList.append(curWidht == 0 ? 20 : curWidht);
                    }
					qDebug() << "save" << sectionSizeList;
                    QByteArray arr;
                    QDataStream ds(&arr, QIODevice::ReadWrite);
                    ds << sectionSizeList; // ������� ��� �������� ����� �� ������� ��������

                    QVariant var;
                    var.setValue(arr);
                    set.setValue(qobject_cast<QTableView*>(wgt)->objectName() + colWidthPostfix_, var);
                }
            }

            if (qobject_cast<QSplitter*>(wgt)) { // ��������� ��������� ���������� �������
                QList< int > splitterSizeList = qobject_cast<QSplitter*>(wgt)->sizes(); // ������� ������ ��� ������� ����������
                if (splitterSizeList.count() > 0) {
                    QByteArray arr;
                    QDataStream ds(&arr, QIODevice::ReadWrite);
                    ds << splitterSizeList; // �������� � ����� ������ �������� ��� ����������

                    QVariant var;
                    var.setValue(arr);
                    set.setValue(qobject_cast<QSplitter*>(wgt)->objectName() + splitterPostfix_, var);
                }
            }

            if (qobject_cast<QGraphicsView*>(wgt)) {
                set.setValue(wgt->objectName() + scrollHorizontalPostfix_, QVariant(qobject_cast<QGraphicsView*>(wgt)->horizontalScrollBar()->value()));
                set.setValue(wgt->objectName() + scrollVerticalPostfix_, QVariant(qobject_cast<QGraphicsView*>(wgt)->verticalScrollBar()->value()));
            }

            QList < QObject* > child_list = wgt->children();
            Q_FOREACH(QObject * next_child, child_list) {
                if (QWidget* wgt = qobject_cast<QWidget*>(next_child)) {
                    save(wgt);
                }
            }
        }


        /** ��������������� ��������� �������.
        *	\param [in] wgt ��������� �� �������������� ������.
        */
        static void restore(QWidget* wgt) {
            checkFilePath();
            QSettings set(filePath_, QSettings::IniFormat);
            set.setIniCodec("UTF-8");

            // ��������������� ������� � ������� ������� �������
            if (qobject_cast<QDialog*>(wgt) || qobject_cast<QMainWindow*>(wgt)) {
                QSize wgtSize = set.value(wgt->objectName() + sizePostfix_, QVariant(wgt->size())).toSize();
                QPoint wgtPos = set.value(wgt->objectName() + posPostfix_, QVariant(wgt->pos())).toPoint(); // point ������������� pos, ��� ��� ��� ���� QPos

                //�������� ���������� ���� � �������� ������
                wgtPos.setX(wgtPos.x() < 0 ? 0 : wgtPos.x());
                wgtPos.setY(wgtPos.y() < 0 ? 0 : wgtPos.y());

                wgt->resize(wgtSize);
                wgt->move(wgtPos);
            }

            if (qobject_cast<QTableView*>(wgt)) {
                QList<int> sectionSizeList;

                QByteArray arr;
                QDataStream ds(&arr, QIODevice::ReadWrite);
                arr = set.value(qobject_cast<QTableView*>(wgt)->objectName() + colWidthPostfix_, QVariant()).toByteArray();
                ds >> sectionSizeList;

                if (qobject_cast<QTableView*>(wgt)->model()) {
                    int col_count = qobject_cast<QTableView*>(wgt)->model()->columnCount(); //���������� ��� ������� �������� ���������� �������
                 
					qDebug() << "restore" << sectionSizeList;

					for (int nn = 0; nn < sectionSizeList.size(); ++nn) {
                        if (nn < col_count)
                            qobject_cast<QTableView*>(wgt)->setColumnWidth(nn, sectionSizeList.at(nn));
                    }
                }
            }

            if (qobject_cast<QSplitter*>(wgt)) { // ��������� ��������� ���������� �������
                QList< int > splitterSizeList; // ������� ������ ��� ������� ����������

                QByteArray arr;
                QDataStream ds(&arr, QIODevice::ReadWrite);
                arr = set.value(qobject_cast<QSplitter*>(wgt)->objectName() + splitterPostfix_, QVariant()).toByteArray();
                ds >> splitterSizeList; // ���������� � ����� ������ �������� ��� ����������

                if (splitterSizeList.count() > 0) {
                    qobject_cast<QSplitter*>(wgt)->setSizes(splitterSizeList);
                }
            }

            if (qobject_cast<QGraphicsView*>(wgt)) {
                const int horValue = set.value(wgt->objectName() + scrollHorizontalPostfix_, QVariant(0)).toInt();
                const int vertValue = set.value(wgt->objectName() + scrollVerticalPostfix_, QVariant(0)).toInt();

                qobject_cast<QGraphicsView*>(wgt)->horizontalScrollBar()->setValue(horValue);
                qobject_cast<QGraphicsView*>(wgt)->verticalScrollBar()->setValue(vertValue);
            }

            //�������� ��� ���� ��������
            QList < QObject* > childList = wgt->children();
            Q_FOREACH(QObject * next_child, childList) {
                if (QWidget* CurWgt = qobject_cast<QWidget*>(next_child)) {
                    restore(CurWgt);
                }
            }
        }

        static void restoreScrollBar(QGraphicsView* wgt) {
            checkFilePath();
            QSettings set(filePath_, QSettings::IniFormat);
            set.setIniCodec("UTF-8"); 
            
            const int horValue = set.value(wgt->objectName() + scrollHorizontalPostfix_, QVariant(0)).toInt();
            const int vertValue = set.value(wgt->objectName() + scrollVerticalPostfix_, QVariant(0)).toInt();

            wgt->horizontalScrollBar()->setValue(horValue);
            wgt->verticalScrollBar()->setValue(vertValue);            
        }

    private:
        /** ��������� ���� � ����� � �����������
        *   ���� �� �����, ������ �������� ��-���������.
        */
        static void checkFilePath() {
            if (filePath_.isEmpty()) {
                filePath_ = QCoreApplication::applicationDirPath() + "/" + QCoreApplication::applicationName() + ".ini";
            }
        }

        static QString filePath_;   //!< ���� � ����� � �����������.        
    };
}
