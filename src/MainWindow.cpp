#include "MainWindow.h"

#include <QtGui/QPen>
#include <QtGui/QTextDocument>
#include <QtPrintSupport/QPrinter>
#include <QtPrintSupport/QPrintDialog>
#include <QtWidgets/QColorDialog>
#include <QtWidgets/QFileDialog>
#include <QtWidgets/QMessageBox>

#include "SettsSingle.h"
#include "TableModelUtils.h"
#include "UtilWidgetParams.h"
#include "UtilWidgetPos.h"
#include "WsProtocol.h"


namespace qt_utils {
	QString WidgetParams::filePath_ = "";	
}


namespace Fdps {
	namespace statistics {

		SettsSingle* SettsSingle::instance_ = nullptr;


		MainWindow::MainWindow(QWidget* parent /*= 0 */)
			: QMainWindow(parent)
			, plansProxyModel_(this)
			, plansSourceModel_(this)
			, histCntrl_(new HistController(this))
			, legendModel_(this)
			, spinBoxDelegate_(this) {
			
			gui_.setupUi(this);
			setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
			mainWindowIcon_.addFile(QStringLiteral(":/icons/statistics.png"), QSize(), QIcon::Normal, QIcon::Off);
			setWindowIcon(mainWindowIcon_);

			setAutoFillBackground(true);

			connect(gui_.tblv_Legend, &QTableView::clicked, this, &MainWindow::legendTableClicked);
			connect(gui_.tblv_Legend, &QTableView::doubleClicked, this, &MainWindow::legendTableDoubleClicked);
			
			connect(&legendModel_, &LegendTableModel::itemChanged, this, &MainWindow::itemChangedInLegend);

			prepareTableView(gui_.tblv_Legend, &legendModel_, false);
			gui_.tblv_Legend->setItemDelegateForColumn(LegendNpsColumn, &spinBoxDelegate_);
			gui_.tblv_Legend->setAutoFillBackground(true);

			QPalette legendPlansTable = gui_.tblv_Legend->palette();
			legendPlansTable.setColor(QPalette::Base, palette().background().color());
			gui_.tblv_Legend->setPalette(legendPlansTable);
			

			connect(gui_.rb_StructSector, &QAbstractButton::toggled, this, &MainWindow::structCheckBoxToggled);
			connect(gui_.rb_StructPoint, &QAbstractButton::toggled, this, &MainWindow::structCheckBoxToggled);
			connect(gui_.rb_StructAirdrom, &QAbstractButton::toggled, this, &MainWindow::structCheckBoxToggled);
			connect(gui_.rb_StructAirway, &QAbstractButton::toggled, this, &MainWindow::structCheckBoxToggled);

			connect(gui_.rb_PlanDaily, &QAbstractButton::toggled, this, &MainWindow::formDataForHistogram);
			connect(gui_.rb_PlanPerform, &QAbstractButton::toggled, this, &MainWindow::formDataForHistogram);
			connect(gui_.rb_PlanCancel, &QAbstractButton::toggled, this, &MainWindow::formDataForHistogram);
			connect(gui_.rb_PlanUnperform, &QAbstractButton::toggled, this, &MainWindow::formDataForHistogram);

			connect(gui_.rb_Total, &QAbstractButton::toggled, this, &MainWindow::formDataForHistogram);
			connect(gui_.rb_Arrival, &QAbstractButton::toggled, this, &MainWindow::formDataForHistogram);
			connect(gui_.rb_Departure, &QAbstractButton::toggled, this, &MainWindow::formDataForHistogram);
			connect(gui_.rb_Transit, &QAbstractButton::toggled, this, &MainWindow::formDataForHistogram);

			connect(gui_.cb_SelectAll, &QAbstractButton::toggled, this, &MainWindow::selectAllCheckBoxToggled);
			connect(gui_.cb_ShowGrid, &QAbstractButton::toggled, this, [&] (const bool & isChecked) {
				SettsSingle::instance()->setGridVisible(isChecked);
				histCntrl_->setGridVisible(isChecked);
			});

			connect(gui_.dte_Date, &QDateEdit::dateChanged, &wsCntrl_, &WsClientController::setDateForPlansRequest);
						
			gui_.sa_Plans->setWidget(histCntrl_->getHistWidget());

			connect(gui_.tb_HistPrint, &QToolButton::clicked, histCntrl_, &HistController::histPrint);
			connect(gui_.tb_HistSaveToFile, &QToolButton::clicked, this, [&] () {
				const QString fileName = gui_.dte_Date->date().toString("yyyy-MM-dd ") + legendTypeToString(curLegendType());
				histCntrl_->histSaveToFile(fileName);
			});

			connect(gui_.tb_TablePrint, &QToolButton::clicked, this, &MainWindow::tablePrintBtnClicked);
			connect(gui_.tb_TableSaveToFile, &QToolButton::clicked, this, &MainWindow::tableSaveToFileBtnClicked);

			connect(gui_.btn_Apply, &QToolButton::clicked, this, &MainWindow::applyBtnClicked);
			connect(gui_.btn_Cancel, &QToolButton::clicked, this, &MainWindow::initSettingsTab);

			connect(gui_.le_Address, &QLineEdit::textChanged, this, &MainWindow::settingsChanged);
			connect(gui_.spb_Port, QOverload<int>::of(&QSpinBox::valueChanged), this, &MainWindow::settingsChanged);
			connect(gui_.le_Path, &QLineEdit::textChanged, this, &MainWindow::settingsChanged);

			//connect(gui_.tblv_Plans, &TableViewWithClick::mouseDoubleClicked, this, &MainWindow::parkingTableDoubleClicked);

			connect(&plansSourceModel_, &PlansTableModel::itemChanged, this, &MainWindow::itemChangedInPlansTable);

			plansProxyModel_.setSourceModel(&plansSourceModel_);

			prepareTableView(gui_.tblv_Plans, &plansProxyModel_, true);

			gui_.tblv_Plans->setItemDelegateForColumn(PlansNpsColumn, &spinBoxDelegate_);
			gui_.tblv_Plans->setAutoFillBackground(true);

			QPalette pltPlansTable = gui_.tblv_Plans->palette();
			pltPlansTable.setColor(QPalette::Base, palette().background().color());
			gui_.tblv_Plans->setPalette(pltPlansTable);

			connect(&wsCntrl_, &WsClientController::sendConnStateChanged, this, [&](const QString& stateStr) {
				statusBar()->showMessage(stateStr);
				});

			connect(this, &MainWindow::needUpdateWsSettings, &wsCntrl_, &WsClientController::needUpdateParams);
						
			connect(&wsCntrl_, &WsClientController::sendPlansInfo, this, [&](const PlansInfo& planInf) {
				plans_ = planInf;

				updateStructData();
				});

			wsCntrl_.startWork();

			initSettingsTab();

			gui_.rb_StructSector->setChecked(true);

			gui_.dte_Date->setDate(QDate::currentDate());



			setCursor(Qt::ArrowCursor);

			qt_utils::WidgetParams::restore(this);
		}


		MainWindow::~MainWindow() {
			qt_utils::WidgetParams::save(this);
		}


		void MainWindow::applyBtnClicked() {

			AppSettings curAppSetts;
			curAppSetts.showGrid_ = gui_.cb_ShowGrid->isChecked();

			curAppSetts.wsSetts_.serverAddr_ = gui_.le_Address->text();
			curAppSetts.wsSetts_.serverPort_ = gui_.spb_Port->value();
			curAppSetts.wsSetts_.serverPath_ = gui_.le_Path->text();
			SettsSingle::instance()->setAppSetts(curAppSetts);

			Q_EMIT needUpdateWsSettings();

			gui_.btn_Apply->setEnabled(false);
			gui_.btn_Cancel->setEnabled(false);
		}


		void MainWindow::structCheckBoxToggled(bool toggled) {
			if (!toggled) {
				return;
			}

			updateStructData();
		}


		void MainWindow::selectAllCheckBoxToggled(bool toggled) {
			for (int nn = 0; nn < legendModel_.rowCount(QModelIndex()); ++nn) {
				const QModelIndex idxName = legendModel_.index(nn, LegendNameColumn);
				const QModelIndex idxNps = legendModel_.index(nn, LegendNpsColumn);

				LegendItem curItem = legendModel_.getItemByIndex(idxName);
				curItem.isChecked = toggled;
				curItem.npsChecked = toggled ? curItem.npsChecked : toggled;
				legendModel_.setData(idxName, QVariant(curItem.isChecked), Qt::EditRole);
				legendModel_.setData(idxNps, QVariant(curItem.npsChecked), Qt::EditRole);
				SettsSingle::instance()->updateLegendItem(curItem, curLegendType());
			}
			SettsSingle::instance()->saveSettings();
			formDataForHistogram();
		}


		void MainWindow::legendTableClicked(const QModelIndex& index)
		{
			if (index.isValid()) {
				if (index.column() == LegendNameColumn || index.column() == LegendMaxPsColumn) {
					LegendItem curItem = legendModel_.getItemByIndex(index);
					
					if (index.column() == LegendNameColumn) {
						curItem.isChecked = !curItem.isChecked;
						legendModel_.setData(index, QVariant(curItem.isChecked), Qt::EditRole);
					} else if (index.column() == LegendMaxPsColumn) {
						curItem.npsChecked = !curItem.npsChecked;
						legendModel_.setData(index, QVariant(curItem.npsChecked), Qt::CheckStateRole);
					}
					SettsSingle::instance()->updateLegendItem(curItem, curLegendType());
					SettsSingle::instance()->saveSettings();
					updateCheckBoxSelectAll();
					formDataForHistogram();
				}
			}
		}


		void MainWindow::legendTableDoubleClicked(const QModelIndex& index)
		{
			if (index.isValid() ){
				if (index.column() == LegendColorColumn) {
					LegendItem curItem = legendModel_.getItemByIndex(index);

					QColorDialog colorDlg(legendModel_.data(index, Qt::BackgroundColorRole).value<QColor>(), this);
					colorDlg.setWindowTitle(QString("Выберите цвет для %1").arg(curItem.name));

					if (colorDlg.exec() == QDialog::Accepted) {				
						curItem.color = colorDlg.currentColor().name();
						legendModel_.setData(index, QVariant(curItem.color), Qt::EditRole);
						SettsSingle::instance()->updateLegendItem(curItem, curLegendType());
						SettsSingle::instance()->saveSettings();
						formDataForHistogram();
					}
				}
			}
		}


		void MainWindow::itemChangedInLegend(const LegendItem& item) {
			SettsSingle::instance()->updateLegendItem(item, curLegendType());
			SettsSingle::instance()->saveSettings();
			formDataForHistogram();
		}

		
		void MainWindow::itemChangedInPlansTable(const LegendItem& item) {
			SettsSingle::instance()->updateLegendItem(item, curLegendType());
			SettsSingle::instance()->saveSettings();
			formDataForHistogram();
		}


		void MainWindow::tablePrintBtnClicked() {
			QTextDocument doc;
			doc.setHtml(formHtmlToFile());

			QPrinter printer(QPrinter::HighResolution);
			printer.setFullPage(true);
			QPrintDialog dlg(&printer, this);

			dlg.setWindowTitle(tr("Печать статистики"));
			if (dlg.exec() == QDialog::Accepted)
				doc.print(&printer);
		}

		
		void MainWindow::tableSaveToFileBtnClicked() {

			QString fileName = QFileDialog::getSaveFileName(( QWidget* ) 0, "Сохранение статистики", QDir::homePath(), "*.pdf");
			if (fileName.isEmpty()) {
				return;
			}
			
			if (QFileInfo(fileName).suffix().isEmpty()) { 
				fileName.append(".pdf");
			}

			QPrinter printer(QPrinter::PrinterResolution);
			printer.setOutputFormat(QPrinter::PdfFormat);
			printer.setPaperSize(QPrinter::A4);
			printer.setOutputFileName(fileName);

			QTextDocument doc;
			doc.setHtml(formHtmlToFile());
			doc.setPageSize(printer.pageRect().size());
			doc.print(&printer);

			QMessageBox::information(this,
				tr("Сохранение статистики"),
				tr("Данные успешно сохранены."));
		}


		void MainWindow::initSettingsTab() {
			gui_.cb_ShowGrid->setChecked(SettsSingle::instance()->getAppSetts().showGrid_);
			histCntrl_->setGridVisible(SettsSingle::instance()->getAppSetts().showGrid_);

			gui_.le_Address->setText(SettsSingle::instance()->getAppSetts().wsSetts_.serverAddr_);
			gui_.spb_Port->setValue(SettsSingle::instance()->getAppSetts().wsSetts_.serverPort_);
			gui_.le_Path->setText(SettsSingle::instance()->getAppSetts().wsSetts_.serverPath_);

			gui_.btn_Apply->setEnabled(false);
			gui_.btn_Cancel->setEnabled(false);
		}


		void MainWindow::settingsChanged() {
			gui_.btn_Apply->setEnabled(true);
			gui_.btn_Cancel->setEnabled(true);
		}


		void MainWindow::updateStructData() {

			blockSignalsForPlansGroup(true);
			gui_.rb_PlanDaily->setChecked(true);
			blockSignalsForPlansGroup(false);

			blockSignalsForTypeGroup(true);
			gui_.rb_Total->setChecked(true);
			blockSignalsForTypeGroup(false);

			gui_.rb_Arrival->setEnabled(gui_.rb_StructSector->isChecked() || gui_.rb_StructAirdrom->isChecked() || gui_.rb_StructAirway->isChecked());
			gui_.rb_Departure->setEnabled(gui_.rb_StructSector->isChecked() || gui_.rb_StructAirdrom->isChecked() || gui_.rb_StructAirway->isChecked());
			gui_.rb_Transit->setEnabled(gui_.rb_StructSector->isChecked() || gui_.rb_StructAirway->isChecked());
			
			formDataForHistogram();
		}


		void MainWindow::formDataForHistogram() {
			LegendItemList allLegendItems = formLegendItems();
			
			const QStringList checkedNames = curCheckedNames();
			
			if (gui_.rb_StructSector->isChecked()) {

				if (gui_.rb_PlanDaily->isChecked()) {

					if (gui_.rb_Total->isChecked()) {
						for (auto it : formLegendItems()) {
							//if (checkedNames.contains(it.name)) {
								allLegendItems.setRawData(it.name, plans_.rcPlans.value(it.name).totalCount.dailyCount);
							//}
							LegendItem curItem = allLegendItems.getLegendForName(it.name);
							curItem.curPs.clear();
							for (auto key : plans_.rcPlans.value(it.name).totalCount.dailyCount.keys()) {
								curItem.curPs.append(plans_.rcPlans.value(it.name).totalCount.dailyCount.value(key));							
							}
							
							allLegendItems.setLegendItem(curItem);
						}
					} else if (gui_.rb_Arrival->isChecked()) {
						//for (auto it : checkedNames) {
						for (auto it : plans_.rcPlans.keys()) {
							allLegendItems.setRawData(it, plans_.rcPlans.value(it).arrCount.dailyCount);
						}
					} else if (gui_.rb_Departure->isChecked()) {
						//for (auto it : checkedNames) {
						for(auto it : plans_.rcPlans.keys()){
							allLegendItems.setRawData(it, plans_.rcPlans.value(it).depCount.dailyCount);
						}
					} else if (gui_.rb_Transit->isChecked()) {
						//for (auto it : checkedNames) {
						for (auto it : plans_.rcPlans.keys()) {
							allLegendItems.setRawData(it, plans_.rcPlans.value(it).transitCount.dailyCount);
						}
					}

				} else if (gui_.rb_PlanPerform->isChecked()) {

					if (gui_.rb_Total->isChecked()) {
						//for (auto it : checkedNames) {
						for (auto it : plans_.rcPlans.keys()) {
							allLegendItems.setRawData(it, plans_.rcPlans.value(it).totalCount.performCount);
						}
					} else if (gui_.rb_Arrival->isChecked()) {
						//for (auto it : checkedNames) {
						for (auto it : plans_.rcPlans.keys()) {
							allLegendItems.setRawData(it, plans_.rcPlans.value(it).arrCount.performCount);
						}
					} else if (gui_.rb_Departure->isChecked()) {
						//for (auto it : checkedNames) {
						for (auto it : plans_.rcPlans.keys()) {
							allLegendItems.setRawData(it, plans_.rcPlans.value(it).depCount.performCount);
						}
					} else if (gui_.rb_Transit->isChecked()) {
						//for (auto it : checkedNames) {
						for (auto it : plans_.rcPlans.keys()) {
							allLegendItems.setRawData(it, plans_.rcPlans.value(it).transitCount.performCount);
						}
					}

				} else if (gui_.rb_PlanCancel->isChecked()) {

					if (gui_.rb_Total->isChecked()) {
						//for (auto it : checkedNames) {
						for (auto it : plans_.rcPlans.keys()) {
							allLegendItems.setRawData(it, plans_.rcPlans.value(it).totalCount.cancelCount);
						}
					} else if (gui_.rb_Arrival->isChecked()) {
						//for (auto it : checkedNames) {
						for (auto it : plans_.rcPlans.keys()) {
							allLegendItems.setRawData(it, plans_.rcPlans.value(it).arrCount.cancelCount);
						}
					} else if (gui_.rb_Departure->isChecked()) {
						//for (auto it : checkedNames) {
						for (auto it : plans_.rcPlans.keys()) {
							allLegendItems.setRawData(it, plans_.rcPlans.value(it).depCount.cancelCount);
						}
					} else if (gui_.rb_Transit->isChecked()) {
						//for (auto it : checkedNames) {
						for (auto it : plans_.rcPlans.keys()) {
							allLegendItems.setRawData(it, plans_.rcPlans.value(it).transitCount.cancelCount);
						}
					}

				} else if (gui_.rb_PlanUnperform->isChecked()) {

					if (gui_.rb_Total->isChecked()) {
						//for (auto it : checkedNames) {
						for (auto it : plans_.rcPlans.keys()) {
							allLegendItems.setRawData(it, plans_.rcPlans.value(it).totalCount.unperformCount);
						}
					} else if (gui_.rb_Arrival->isChecked()) {
						//for (auto it : checkedNames) {
						for (auto it : plans_.rcPlans.keys()) {
							allLegendItems.setRawData(it, plans_.rcPlans.value(it).arrCount.unperformCount);
						}
					} else if (gui_.rb_Departure->isChecked()) {
						//for (auto it : checkedNames) {
						for (auto it : plans_.rcPlans.keys()) {
							allLegendItems.setRawData(it, plans_.rcPlans.value(it).depCount.unperformCount);
						}
					} else if (gui_.rb_Transit->isChecked()) {
						//for (auto it : checkedNames) {
						for (auto it : plans_.rcPlans.keys()) {
							allLegendItems.setRawData(it, plans_.rcPlans.value(it).transitCount.unperformCount);
						}
					}

				}
				updateLegend(allLegendItems, "Секторы");
			}
			else if (gui_.rb_StructPoint->isChecked()) {

				if (gui_.rb_PlanDaily->isChecked()) {
					//for (auto it : checkedNames) {
					for (auto it : plans_.pointPlans.keys()) {
						allLegendItems.setRawData(it, plans_.pointPlans.value(it).dailyCount);
					}
				}
				else if (gui_.rb_PlanPerform->isChecked()) {
					//for (auto it : checkedNames) {
					for (auto it : plans_.pointPlans.keys()) {
						allLegendItems.setRawData(it, plans_.pointPlans.value(it).performCount);
					}
				}
				else if (gui_.rb_PlanCancel->isChecked()) {
					//for (auto it : checkedNames) {
					for (auto it : plans_.pointPlans.keys()) {
						allLegendItems.setRawData(it, plans_.pointPlans.value(it).cancelCount);
					}
				}
				else if (gui_.rb_PlanUnperform->isChecked()) {
					//for (auto it : checkedNames) {
					for (auto it : plans_.pointPlans.keys()) {
						allLegendItems.setRawData(it, plans_.pointPlans.value(it).unperformCount);
					}
				}

				updateLegend(allLegendItems, "Точки");
			}
			else if (gui_.rb_StructAirdrom->isChecked()) {

				if (gui_.rb_PlanDaily->isChecked()) {

					if (gui_.rb_Total->isChecked()) {
						//for (auto it : checkedNames) {
						for (auto it : plans_.airdromPlans.keys()) {
							allLegendItems.setRawData(it, plans_.airdromPlans.value(it).totalCount.dailyCount);
						}
					}
					else if (gui_.rb_Arrival->isChecked()) {
						//for (auto it : checkedNames) {
						for (auto it : plans_.airdromPlans.keys()) {
							allLegendItems.setRawData(it, plans_.airdromPlans.value(it).arrCount.dailyCount);
						}
					}
					else if (gui_.rb_Departure->isChecked()) {
						//for (auto it : checkedNames) {
						for (auto it : plans_.airdromPlans.keys()) {
							allLegendItems.setRawData(it, plans_.airdromPlans.value(it).depCount.dailyCount);
						}
					}

				}
				else if (gui_.rb_PlanPerform->isChecked()) {

					if (gui_.rb_Total->isChecked()) {
						//for (auto it : checkedNames) {
						for (auto it : plans_.airdromPlans.keys()) {
							allLegendItems.setRawData(it, plans_.airdromPlans.value(it).totalCount.performCount);
						}
					}
					else if (gui_.rb_Arrival->isChecked()) {
						//for (auto it : checkedNames) {
						for (auto it : plans_.airdromPlans.keys()) {
							allLegendItems.setRawData(it, plans_.airdromPlans.value(it).arrCount.performCount);
						}
					}
					else if (gui_.rb_Departure->isChecked()) {
						//for (auto it : checkedNames) {
						for (auto it : plans_.airdromPlans.keys()) {
							allLegendItems.setRawData(it, plans_.airdromPlans.value(it).depCount.performCount);
						}
					}

				}
				else if (gui_.rb_PlanCancel->isChecked()) {

					if (gui_.rb_Total->isChecked()) {
						//for (auto it : checkedNames) {
						for (auto it : plans_.airdromPlans.keys()) {
							allLegendItems.setRawData(it, plans_.airdromPlans.value(it).totalCount.cancelCount);
						}
					}
					else if (gui_.rb_Arrival->isChecked()) {
						//for (auto it : checkedNames) {
						for (auto it : plans_.airdromPlans.keys()) {
							allLegendItems.setRawData(it, plans_.airdromPlans.value(it).arrCount.cancelCount);
						}
					}
					else if (gui_.rb_Departure->isChecked()) {
						//for (auto it : checkedNames) {
						for (auto it : plans_.airdromPlans.keys()) {
							allLegendItems.setRawData(it, plans_.airdromPlans.value(it).depCount.cancelCount);
						}
					}

				}
				else if (gui_.rb_PlanUnperform->isChecked()) {

					if (gui_.rb_Total->isChecked()) {
						//for (auto it : checkedNames) {
						for (auto it : plans_.airdromPlans.keys()) {
							allLegendItems.setRawData(it, plans_.airdromPlans.value(it).totalCount.unperformCount);
						}
					}
					else if (gui_.rb_Arrival->isChecked()) {
						//for (auto it : checkedNames) {
						for (auto it : plans_.airdromPlans.keys()) {
							allLegendItems.setRawData(it, plans_.airdromPlans.value(it).arrCount.unperformCount);
						}
					}
					else if (gui_.rb_Departure->isChecked()) {
						//for (auto it : checkedNames) {
						for (auto it : plans_.airdromPlans.keys()) {
							allLegendItems.setRawData(it, plans_.airdromPlans.value(it).depCount.unperformCount);
						}
					}

				}
				updateLegend(allLegendItems, "Аэродромы");
			}
			else if (gui_.rb_StructAirway->isChecked()) {
				if (gui_.rb_PlanDaily->isChecked()) {
					
					if (gui_.rb_Total->isChecked()) {

						for (auto it : plans_.airwayPlans.keys()) {
							allLegendItems.setRawData(it, plans_.airwayPlans.value(it).totalCount.dailyCount);
						}

					} else if (gui_.rb_Arrival->isChecked()) {

						for (auto it : plans_.airwayPlans.keys()) {
							allLegendItems.setRawData(it, plans_.airwayPlans.value(it).arrCount.dailyCount);
						}

					} else if (gui_.rb_Departure->isChecked()) {
						
						for (auto it : plans_.airwayPlans.keys()) {
							allLegendItems.setRawData(it, plans_.airwayPlans.value(it).depCount.dailyCount);
						}

					} else if (gui_.rb_Transit->isChecked()) {

						for (auto it : plans_.airwayPlans.keys()) {
							allLegendItems.setRawData(it, plans_.airwayPlans.value(it).transitCount.dailyCount);
						}

					}

				} else if (gui_.rb_PlanPerform->isChecked()) {

					if (gui_.rb_Total->isChecked()) {
						
						for (auto it : plans_.airwayPlans.keys()) {
							allLegendItems.setRawData(it, plans_.airwayPlans.value(it).totalCount.performCount);
						}

					} else if (gui_.rb_Arrival->isChecked()) {
						
						for (auto it : plans_.airwayPlans.keys()) {
							allLegendItems.setRawData(it, plans_.airwayPlans.value(it).arrCount.performCount);
						}

					} else if (gui_.rb_Departure->isChecked()) {
						
						for (auto it : plans_.airwayPlans.keys()) {
							allLegendItems.setRawData(it, plans_.airwayPlans.value(it).depCount.performCount);
						}

					} else if (gui_.rb_Transit->isChecked()) {
						
						for (auto it : plans_.airwayPlans.keys()) {
							allLegendItems.setRawData(it, plans_.airwayPlans.value(it).transitCount.performCount);
						}

					}

				} else if (gui_.rb_PlanCancel->isChecked()) {

					if (gui_.rb_Total->isChecked()) {
						
						for (auto it : plans_.airwayPlans.keys()) {
							allLegendItems.setRawData(it, plans_.airwayPlans.value(it).totalCount.cancelCount);
						}

					} else if (gui_.rb_Arrival->isChecked()) {
						
						for (auto it : plans_.airwayPlans.keys()) {
							allLegendItems.setRawData(it, plans_.airwayPlans.value(it).arrCount.cancelCount);
						}

					} else if (gui_.rb_Departure->isChecked()) {
						
						for (auto it : plans_.airwayPlans.keys()) {
							allLegendItems.setRawData(it, plans_.airwayPlans.value(it).depCount.cancelCount);
						}

					} else if (gui_.rb_Transit->isChecked()) {
						
						for (auto it : plans_.airwayPlans.keys()) {
							allLegendItems.setRawData(it, plans_.airwayPlans.value(it).transitCount.cancelCount);
						}

					}

				} else if (gui_.rb_PlanUnperform->isChecked()) {

					if (gui_.rb_Total->isChecked()) {
						
						for (auto it : plans_.airwayPlans.keys()) {
							allLegendItems.setRawData(it, plans_.airwayPlans.value(it).totalCount.unperformCount);
						}

					} else if (gui_.rb_Arrival->isChecked()) {
						
						for (auto it : plans_.airwayPlans.keys()) {
							allLegendItems.setRawData(it, plans_.airwayPlans.value(it).arrCount.unperformCount);
						}

					} else if (gui_.rb_Departure->isChecked()) {
						
						for (auto it : plans_.airwayPlans.keys()) {
							allLegendItems.setRawData(it, plans_.airwayPlans.value(it).depCount.unperformCount);
						}

					} else if (gui_.rb_Transit->isChecked()) {
						
						for (auto it : plans_.airwayPlans.keys()) {
							allLegendItems.setRawData(it, plans_.airwayPlans.value(it).transitCount.unperformCount);
						}

					}
				}

				updateLegend(allLegendItems, "Трассы");
			}


			LegendItemList checkedLegendItems;
			for (auto it : allLegendItems) {
				if (checkedNames.contains(it.name)) {
					checkedLegendItems.append(it);
				}
				SettsSingle::instance()->updateLegendItem(it, curLegendType());
			}
			SettsSingle::instance()->saveSettings();
			histCntrl_->setLegendType(curLegendType());
			histCntrl_->updatePlot(checkedLegendItems);

			
			const bool colorNps = (curLegendType() == LegendRc && gui_.rb_PlanDaily->isChecked() && gui_.rb_Total->isChecked());
			plansSourceModel_.setColorNps(colorNps);
			gui_.tblv_Plans->setColumnHidden(PlansNpsColumn, !colorNps);
			plansSourceModel_.setLegendItemList(allLegendItems, curLegendType());
		}


		void MainWindow::blockSignalsForPlansGroup(bool block) {
			gui_.rb_PlanDaily->blockSignals(block);
			gui_.rb_PlanPerform->blockSignals(block);
			gui_.rb_PlanCancel->blockSignals(block);
			gui_.rb_PlanUnperform->blockSignals(block);
		}


		void MainWindow::blockSignalsForTypeGroup(bool block) {
			gui_.rb_Total->blockSignals(block);
			gui_.rb_Arrival->blockSignals(block);
			gui_.rb_Departure->blockSignals(block);
			gui_.rb_Transit->blockSignals(block);
		}


		void MainWindow::updateLegend(const LegendItemList& items, const QString& title) {
			gui_.gb_Struct->setTitle(title);

			const bool colorNps = ( curLegendType() == LegendRc && gui_.rb_PlanDaily->isChecked() && gui_.rb_Total->isChecked() );

			legendModel_.setLegendItemList(items);

			if (!colorNps && !gui_.tblv_Legend->isColumnHidden(LegendNpsColumn)) {
				qt_utils::WidgetParams::save(gui_.tblv_Legend);
			}
			
			if (colorNps && gui_.tblv_Legend->isColumnHidden(LegendNpsColumn)) {
				qt_utils::WidgetParams::restore(gui_.tblv_Legend);
			}

			gui_.tblv_Legend->setColumnHidden(LegendNpsColumn, !colorNps);
			gui_.tblv_Legend->setColumnHidden(LegendMaxPsColumn, !colorNps);
			
			updateCheckBoxSelectAll();
		}


		LegendType MainWindow::curLegendType() const {
			if (gui_.rb_StructSector->isChecked()) {
				return LegendType::LegendRc;
			}
			else if (gui_.rb_StructAirdrom->isChecked()) {
				return LegendType::LegendAirport;
			}
			else if (gui_.rb_StructAirway->isChecked()) {
				return LegendType::LegendAirway;
			}
			else if (gui_.rb_StructPoint->isChecked()) {
				return LegendType::LegendPoint;
			}

			return LegendType::LegendUnknwn;
		}


		QStringList MainWindow::curCheckedNames() const {
			QStringList retValue;

			for (auto it : formLegendItems()) {
				if (it.isChecked) {
					retValue.append(it.name);
				}
			}

			return retValue;
		}


		LegendItemList MainWindow::formLegendItems() const {
			if (gui_.rb_StructSector->isChecked()) {
				return SettsSingle::instance()->getAppSetts().getLegendItemsForNames(
					plans_.rcPlans.keys(), LegendType::LegendRc);
			}
			else if (gui_.rb_StructAirdrom->isChecked()) {
				return SettsSingle::instance()->getAppSetts().getLegendItemsForNames(
					plans_.airdromPlans.keys(), LegendType::LegendAirport);
			}
			else if (gui_.rb_StructAirway->isChecked()) {
				return SettsSingle::instance()->getAppSetts().getLegendItemsForNames(
					plans_.airwayPlans.keys(), LegendType::LegendAirway);
			}
			else if (gui_.rb_StructPoint->isChecked()) {
				return SettsSingle::instance()->getAppSetts().getLegendItemsForNames(
					plans_.pointPlans.keys(), LegendType::LegendPoint);
			}

			return LegendItemList();
		}


		void MainWindow::updateCheckBoxSelectAll() {

			int checkedCount = curCheckedNames().size();

			gui_.cb_SelectAll->blockSignals(true);
			if (checkedCount == formLegendItems().size() && checkedCount > 0) {
				gui_.cb_SelectAll->setChecked(true);
			}
			else {
				gui_.cb_SelectAll->setChecked(false);
			}
			gui_.cb_SelectAll->blockSignals(false);
		}


		QString MainWindow::formHtmlToFile() {
			QString structName;
			if (gui_.rb_StructSector->isChecked()) {
				structName = "по секторам";
			} else if (gui_.rb_StructAirdrom->isChecked()) {
				structName = "по аэродромам";
			} else if (gui_.rb_StructPoint->isChecked()) {
				structName = "по точкам";
			} else if (gui_.rb_StructAirway->isChecked()) {
				structName = "по трассам";
			}

			QString plansName;
			if (gui_.rb_PlanDaily->isChecked()) {
				plansName = "суточные";
			} else if (gui_.rb_PlanCancel->isChecked()) {
				plansName = "отмененные";
			} else if (gui_.rb_PlanPerform->isChecked()) {
				plansName = "выполненные";
			} else if (gui_.rb_PlanUnperform->isChecked()) {
				plansName = "не выполненные";
			}
			

			QString typeName;
			if (gui_.rb_Total->isChecked()) {
				typeName = "все";
			} else if (gui_.rb_Departure->isChecked()) {
				typeName = "вылет";
			} else if (gui_.rb_Arrival->isChecked()) {
				typeName = "прилет";
			} else if (gui_.rb_Transit->isChecked()) {
				typeName = "транзит";
			}


			QString retValue = "<font size=\"5\" face=\"verdana\" color=\"black\">";
			retValue += QString("<p>Дата: %1.</p>").arg(gui_.dte_Date->date().toString("yyyy-MM-dd"));
			retValue += QString("<p>Статистика: %1.</p>").arg(structName);
			retValue += QString("<p>Планы: %1 / %2.</p>").arg(typeName).arg(plansName);
			retValue +="<p></p>";
			retValue +="<p></p>";

			retValue += tableToHtml(&plansSourceModel_, gui_.tblv_Plans->isColumnHidden(PlansNpsColumn) ? QList<int>{PlansNpsColumn} : QList<int>());

			return retValue;
		}
	}
}